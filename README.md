# commons-python

This is the library used to share code between Python modules.

## Requirements

- git
- docker

## Local development

*Commands must be run from the root of this repo.*  

### Execute

You cannot execute the library itself. It is inteded to be imported in a module.

### Run the tests

`./localci/test-unit.sh`

Offline:  
It is possible to run tests offline, but you need to run the command while being online at least once.

```
Delivered with 💓 by


                                                                                            
              /')                                                                           
            /' /' ____     O  ____     ,____     ____     ____     ____     ____     ____   
         -/'--' /'    )  /' /'    )   /'    )  /'    )  /'    )--/'    )--/'    )  /'    )--
        /'    /(___,/' /' /'    /'  /'    /' /'    /'  '---,    '---,   /(___,/'  '---,     
      /(_____(________(__(___,/(__/'    /(__(___,/(__(___,/   (___,/   (________(___,/      
    /'                      /'                                                              
  /'                /     /'                                                                
/'                 (___,/'                                                                  
```