import pandas as pd
import numpy as np 

from cryptobot_commons.strategy_result import StrategyResult
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.signal import Signal

def check_evaluation_input(strategy_result:StrategyResult, crypto_currency_pair:CryptoCurrencyPair):
        if type(strategy_result) != StrategyResult:
            raise(TypeError("Error: StrategiesResultsEvaluator - The strategy_result must be of type StrategyResult."))
        if type(crypto_currency_pair) != CryptoCurrencyPair:
            raise(TypeError("Error: StrategiesResultsEvaluator - The crypto_currency_pair must be of type CryptoCurrencyPair."))

def is_upward_trend(returns_at_matched_date):
    return returns_at_matched_date > StrategyResultEvaluator.NEUTRAL_THRESHOLD

def is_downward_trend(returns_at_matched_date):
    return returns_at_matched_date < -StrategyResultEvaluator.NEUTRAL_THRESHOLD

def is_neutral_trend(returns_at_matched_date):
    return -StrategyResultEvaluator.NEUTRAL_THRESHOLD <= returns_at_matched_date <= StrategyResultEvaluator.NEUTRAL_THRESHOLD

def match_with_closest_quotation(date_to_match, crypto_currency_pair):
    i=0
    for quotation_date in crypto_currency_pair.close.data.index:
        if quotation_date > date_to_match:
            i-=1
            break
        else:
            i+=1
    return i

def check_if_add_date_is_ok(strategy_result, crypto_currency_pair):
    if strategy_result.add_date<min(crypto_currency_pair.close.data.index):
        raise(ValueError("Error: The strategy_result.add_date is before the first date of the crypto_currency_pair."))
    elif strategy_result.add_date>max(crypto_currency_pair.close.data.index):
        raise(ValueError("Error: The strategy_result.add_date is after the last date of the crypto_currency_pair."))

class StrategyResultEvaluator:
    
    NEUTRAL_THRESHOLD = 0.0033

    @staticmethod
    def evaluate_the_signal(strategy_result:StrategyResult, crypto_currency_pair:CryptoCurrencyPair):
        """This method will evaluate a strategy by comparing the forecasted signal with the realized market.

        Arguments:
            strategy_result {StrategyResult} -- The StrategyResult that contains the signal we want to compare.
            crypto_currency_pair {CryptoCurrencyPair} -- The CryptoCurrencyPair associated, with the close price of the realized market.
        """
        return strategy_result.signal == StrategyResultEvaluator.get_true_signal(strategy_result, crypto_currency_pair)

    @staticmethod
    def get_true_signal(strategy_result:StrategyResult, crypto_currency_pair:CryptoCurrencyPair):

        check_evaluation_input(strategy_result, crypto_currency_pair)
        check_if_add_date_is_ok(strategy_result, crypto_currency_pair)

        returns_5_periods = crypto_currency_pair.close.asset_return(5)
        # First we need to match the strategy result date with the closest stock quotation
        i = match_with_closest_quotation(strategy_result.add_date, crypto_currency_pair)

        # Then, based on the found quotation, we can check if the result is correct or not
        if is_upward_trend(returns_5_periods.iloc[i+5]):
            return Signal.LONG
        elif is_downward_trend(returns_5_periods.iloc[i+5]):
            return Signal.SHORT
        elif is_neutral_trend(returns_5_periods.iloc[i+5]):
            return Signal.NEUTRAL
        else:
            raise(Exception("Couldn't determine if the trend is upward, downward, or neutral."))