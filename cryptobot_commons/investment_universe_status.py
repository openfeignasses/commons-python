from cryptobot_commons.investment_universe import InvestmentUniverse


class InvestmentUniverseStatus:
    def __init__(self, investment_universe_status_raw: list):
        if not isinstance(investment_universe_status_raw, list):
            raise Exception("investment_universe_status_raw must be of type list.")
        for currency_pair_status in investment_universe_status_raw:
            if not isinstance(currency_pair_status["name"], InvestmentUniverse):
                raise Exception("All currencies pairs must be of type InvestmentUniverse.")

        self.status = {}
        for currency_pair_status in investment_universe_status_raw:
            self.status[currency_pair_status["name"]] = currency_pair_status["status"]

    def set_status(self, currency_pair: InvestmentUniverse, new_status: str):
        self.status[currency_pair] = new_status
        return self

    def get_status(self, currency_pair: InvestmentUniverse):
        return self.status[currency_pair]
