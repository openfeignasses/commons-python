from cryptobot_commons.confusion_matrix import ConfusionMatrix


class ClassificationMetrics:

    def __init__(self, confusion_matrix: ConfusionMatrix, consider_neutral=True):
        self.confusion_matrix = confusion_matrix
        self.consider_neutral = consider_neutral

    def compute_f1_score(self) -> float:
        precision = self.compute_precision()
        recall = self.compute_recall()
        if (precision + recall) == 0:
            return 0.0
        else:
            return 2 * (precision * recall) / (precision + recall)

    def compute_precision(self) -> float:
        precision_long = self.__compute_one_precision(signal='LONG')
        precision_neutral = self.__compute_one_precision(signal='NEUTRAL')
        precision_short = self.__compute_one_precision(signal='SHORT')
        if self.consider_neutral:
            return (precision_long + precision_neutral + precision_short) / 3
        else:
            return (precision_long + precision_short) / 2

    def __compute_one_precision(self, signal: str) -> float:
        sum_col = self.confusion_matrix.matrix.sum(axis=1)
        if sum_col[str(signal) + '_PRED'] == 0:
            return 0
        else:
            return self.confusion_matrix.matrix.loc[signal + '_PRED', \
                                                    signal + '_TRUE'] / sum_col[signal + '_PRED']

    def compute_recall(self) -> float:
        recall_long = self.__compute_one_recall(signal='LONG')
        recall_neutral = self.__compute_one_recall(signal='NEUTRAL')
        recall_short = self.__compute_one_recall(signal='SHORT')
        if self.consider_neutral:
            return (recall_long + recall_neutral + recall_short) / 3
        else:
            return (recall_long + recall_short) / 2

    def __compute_one_recall(self, signal: str) -> float:
        sum_rows = self.confusion_matrix.matrix.sum(axis=0)
        if sum_rows[str(signal) + '_TRUE'] == 0:
            return 0
        else:
            return self.confusion_matrix.matrix.loc[signal + '_PRED', \
                                                    signal + '_TRUE'] / sum_rows[signal + '_TRUE']
