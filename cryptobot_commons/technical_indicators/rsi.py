import pandas as pd
import time


class RSI:

    def compute(self, period: int, data: pd.Series):
        gain, loss = self.__compute_gain_loss(data)
        avg_gain, avg_loss = self.__compute_avg_gain_loss(period, gain, loss)
        uncorrected_rsi = self.__compute_rsi(avg_gain, avg_loss)
        return self.__correct_no_gain_no_loss_rsi(avg_gain, avg_loss, uncorrected_rsi)

    def __correct_no_gain_no_loss_rsi(self, avg_gain: pd.Series, avg_loss: pd.Series, rsi: pd.Series):
        rsi.loc[(avg_loss == 0) & (avg_gain == 0)] = 50
        return rsi

    def __compute_rsi(self, avg_gain: pd.Series, avg_loss: pd.Series):
        return pd.Series(100 - (100 / (1 + (avg_gain / avg_loss))))

    def __compute_avg_gain_loss(self, period, gain: pd.Series, loss: pd.Series):
        return gain.rolling(window=period).mean(), loss.rolling(window=period).mean()

    def __compute_gain_loss(self, data: pd.Series):
        change = data - data.shift(periods=1)
        gain, loss = change, change.abs()
        gain.loc[change < 0] = 0
        loss.loc[change > 0] = 0
        return gain, loss
