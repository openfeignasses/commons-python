from cryptobot_commons.crypto_currency_pair_builder import CryptoCurrencyPairBuilder
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs
from cryptobot_commons.repositories import StockQuotationsRepository
from cryptobot_commons.repositories.repository import Repository


class CryptoCurrencyPairsRetriever:

    repository: Repository
    stock_quotations_for_currency_raw_list: list

    DEFAULT_LIMIT = 100

    def __init__(self, mode: str, name: str, *args, **kwargs):
        if not mode:
            raise Exception("mode parameter must be specified.")
        limit = kwargs.get('limit', CryptoCurrencyPairsRetriever.DEFAULT_LIMIT)
        self.repository = StockQuotationsRepository()
        self.stock_quotations_for_currency_raw_list = []

        if mode == "ALL":
            distinct_currency_pairs = self.repository.get_distinct_currency_pairs()
            for result in distinct_currency_pairs:
                currency_pair_name = result["currency_pair"]
                self.stock_quotations_for_currency_raw_list.append({
                    currency_pair_name: self.repository.get_by_currency(currency_pair_name)
                })
        elif mode == "LAST_HOURS":
            distinct_currency_pairs = self.repository.get_distinct_currency_pairs()
            for result in distinct_currency_pairs:
                currency_pair_name = result["currency_pair"]
                self.stock_quotations_for_currency_raw_list.append({
                    currency_pair_name: self.repository.get_by_currency_last_hours(currency_pair_name, limit)
                })
        elif mode == "BY_CURRENCY":
            self.stock_quotations_for_currency_raw_list.append({
                name: self.repository.get_by_currency(name)
            })
        else:
            raise Exception("The mode "+mode+" is invalid")

    def execute(self) -> CryptoCurrencyPairs:
        list_of_crypto_currency_pairs = []
        for stock_quotations_for_currency_raw in self.stock_quotations_for_currency_raw_list:
            name = next(iter(stock_quotations_for_currency_raw))
            list_of_crypto_currency_pairs.append(
                CryptoCurrencyPairBuilder(
                    name,
                    stock_quotations_for_currency_raw[name]
                ).build()
            )
        return CryptoCurrencyPairs(list_of_crypto_currency_pairs)
