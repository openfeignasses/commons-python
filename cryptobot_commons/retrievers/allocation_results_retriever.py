from cryptobot_commons.allocation_results import AllocationResults
from cryptobot_commons.repositories import AllocationsResultsRepository


class AllocationResultsRetriever:

    DEFAULT_LIMIT = 365*4

    def __init__(self, mode: str, *args, **kwargs):
        if not mode:
            raise Exception("mode parameter must be specified.")
        limit = kwargs.get('limit', AllocationResultsRetriever.DEFAULT_LIMIT)

        self.repository = AllocationsResultsRepository()
        if mode == "ALL":
            self.allocation_results_raw = self.repository.get_all()
        elif mode == "LAST_EXECUTION":
            self.allocation_results_raw = self.repository.get_last_execution()
        elif mode == "LIMITED_ORDERED":
            self.allocation_results_raw = self.repository.get_limited_ordered(limit)
        else:
            raise Exception("The mode "+mode+" is invalid")

    def execute(self) -> AllocationResults:
        return AllocationResults(self.allocation_results_raw)
