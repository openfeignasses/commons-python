from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.repositories import StrategiesResultsRepository
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.signal import Signal


class StrategiesResultsRetriever:

    DEFAULT_LIMIT = 365*4

    def __init__(self, mode: str, *args, **kwargs):
        strategy_name = kwargs.get('strategy_name')
        limit = kwargs.get('limit', StrategiesResultsRetriever.DEFAULT_LIMIT)

        if not mode:
            raise Exception("mode parameter must be specified.")
        self.repository = StrategiesResultsRepository()
        if mode == "ALL":
            self.strategies_results_raw = self.repository.get_all()
        elif mode == "LAST_EXECUTION":
            self.strategies_results_raw = self.repository.get_last_execution()
        elif mode == "LIMITED_ORDERED":
            self.strategies_results_raw = self.repository.get_limited_ordered(limit)
        elif mode == "BY_STRATEGY":
            self.strategies_results_raw = self.repository.get_by_strategy(strategy_name)
        else:
            raise Exception("The mode "+mode+" is invalid")

    def execute(self) -> StrategiesResults:
        self.convert_signals_to_enum()
        self.convert_currencies_pairs_to_enum()
        return StrategiesResults(self.strategies_results_raw)

    def convert_signals_to_enum(self):
        for index in range(len(self.strategies_results_raw)):
            signal_string = self.strategies_results_raw[index]["signal"]
            self.strategies_results_raw[index]["signal"] = Signal[signal_string]

    def convert_currencies_pairs_to_enum(self):
        for index in range(len(self.strategies_results_raw)):
            currency_pair_string = self.strategies_results_raw[index]["currency_pair"]
            self.strategies_results_raw[index]["currency_pair"] = InvestmentUniverse[currency_pair_string]
