from cryptobot_commons.allocation_results import AllocationResults
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus
from cryptobot_commons.repositories import InvestmentUniverseStatusRepository


class InvestmentUniverseStatusRetriever:

    def __init__(self, mode: str):
        if not mode:
            raise Exception("mode parameter must be specified.")
        self.repository = InvestmentUniverseStatusRepository()
        if mode == "ALL":
            self.investment_universe_status_raw = self.repository.get_all()
        elif mode == "TRADABLE":
            self.investment_universe_status_raw = self.repository.get_tradable()
        elif mode == "NON_TRADABLE":
            self.investment_universe_status_raw = self.repository.get_non_tradable()
        elif mode == "CRASHING":
            self.investment_universe_status_raw = self.repository.get_crashing()
        else:
            raise Exception("The mode "+mode+" is invalid")

    def execute(self) -> InvestmentUniverseStatus:
        self.convert_currencies_pairs_to_enum()
        return InvestmentUniverseStatus(self.investment_universe_status_raw)

    def convert_currencies_pairs_to_enum(self):
        for index in range(len(self.investment_universe_status_raw)):
            currency_pair = self.investment_universe_status_raw[index]["name"]
            self.investment_universe_status_raw[index]["name"] = InvestmentUniverse[currency_pair]
