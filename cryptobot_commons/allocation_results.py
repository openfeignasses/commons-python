from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.allocation_result import AllocationResult

def check_input(allocation_results):
    if not isinstance(allocation_results, list):
        raise Exception("The parameter allocation_results must be a list.")
    for element in allocation_results:
        if (type(element) != AllocationResult) and (type(element) != dict):
            raise Exception("Each parameter of allocation_results must be a dict or a AllocationResult.")

class AllocationResults:

    def __init__(self, allocation_results: list):
        check_input(allocation_results)
        self.results = []
        for allocation_result in allocation_results:
            if type(allocation_result) == dict:
                self.results.append(AllocationResult(allocation_result))
            elif type(allocation_result) == AllocationResult:
                self.results.append(allocation_result)
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        for result in self.results:
            if not isinstance(result, AllocationResult):
                raise (Exception("All elements must be of type AllocationResult."))

