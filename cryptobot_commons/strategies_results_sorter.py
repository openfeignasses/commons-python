from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.strategy_result import StrategyResult

class StrategiesResultsSorter:

    def __init__(self, list_strategies_results:list):
        self._list_strategies_results = list_strategies_results
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        if type(self._list_strategies_results) != list:
            raise(Exception("The list_strategies_results must be of type list."))
        for result in self._list_strategies_results:
            if type(result) != StrategiesResults:
                raise(Exception("Each element of the list_strategies_results attribute must be of type StrategiesResults."))
    
    def sort(self):
        sorter = {}
        strategies_results_sorted = []

        for strategies_results in self._list_strategies_results:
            for strategie_result in strategies_results.results:
                if strategie_result.currency_pair.name not in sorter:
                    sorter[strategie_result.currency_pair.name] = [strategie_result]
                else:
                    sorter[strategie_result.currency_pair.name].append(strategie_result)

        for currency_pair_name in sorter:
            strategies_results_sorted.append(StrategiesResults(sorter[currency_pair_name]))

        return strategies_results_sorted
