import pandas as pd
import time
from cryptobot_commons.technical_indicators import RSI


def check_inputs(name: str, data: pd.Series):
    if not isinstance(name, str):
        raise ValueError("The TimeSeries name parameter must be a string.")
    if "_EUR" not in name:
        raise ValueError("The TimeSeries name must contain '_EUR'.")
    if data.isnull().values.any():
        raise ValueError("The data attribute of TimeSeries must have no missing data.")


class TimeSeries:

    def __init__(self, name: str, data: pd.Series):
        check_inputs(name, data)

        self.name = name
        self.data = data
        self.data.name = name
        try:
            self.data = self.data.astype(float)
        except:
            raise TypeError("TimeSeries: Impossible to convert input data into float for the " + str(name))
        self.start_date = self.data.index[0]
        self.end_date = self.data.index[-1]

        self.check_initialized_attributes()

    def check_initialized_attributes(self):
        if not self.start_date < self.end_date:
            raise ValueError("The start_date attributes of TimeSeries must be strictly less than the end_date attribute.")

    # Statistical indicators

    def asset_return(self, period: int) -> pd.Series:
        return self.data.pct_change(period)

    def expected_asset_return(self, period_change: int, period_expectation: int) -> pd.Series:
        return self.data.pct_change(period_change).rolling(window=period_expectation).mean()

    def sma(self, period: int) -> pd.Series:
        return self.data.rolling(window=period).mean()

    def ema(self, period: int) -> pd.Series:
        return self.data.ewm(span=period, adjust=False).mean()

    # Financial Indicators

    def volatility(self, period: int) -> pd.Series:
        return self.asset_return(1).rolling(window=period).std()

    # Technical Analysis Indicators

    def macd(self, short_period: int, long_period: int) -> pd.Series:
        return self.ema(short_period) - self.ema(long_period)

    def macd_ema(self, short_period: int, long_period: int, period_ema: int) -> pd.Series:
        return self.macd(short_period, long_period).ewm(span=period_ema, adjust=False).mean()

    def rsi(self, period: int):
        return RSI().compute(period, self.data)

    def slow_stochastic_indicator(self, period_k: int, mean_period_k: int, mean_period_d: int):
        rolling_min = self.data.rolling(period_k).min()
        rolling_max = self.data.rolling(period_k).max()
        fast_k = 100 * (self.data - rolling_min) / (rolling_max - rolling_min)
        slow_k = fast_k.rolling(window=mean_period_k).mean()
        slow_d = slow_k.rolling(window=mean_period_d).mean()
        return slow_k, slow_d
