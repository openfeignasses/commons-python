from enum import Enum


class InvestmentUniverse(Enum):
    """
    This class defines the tickers of pairs we can trade with.
    The first element is the ticker and the second element is its index.
    """
    ETH_EUR = 1
    GNO_EUR = 2
    ETC_EUR = 3
    REP_EUR = 4
    XLM_EUR = 5
    EOS_EUR = 6
    ZEC_EUR = 7
    XMR_EUR = 8
    XRP_EUR = 9
    BTC_EUR = 10
    LTC_EUR = 11
