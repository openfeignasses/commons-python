def type_checking(func):
    def check_type(*args):
        for i, funct_input in enumerate(func.__annotations__.keys()):
            check_one_argument(args[i], func.__annotations__[funct_input], funct_input)
        return func(*args)
    return check_type

def check_one_argument(value, target_type, argument_name):
    if type(value) != target_type:
        raise TypeError('Error: ' + str(argument_name) + ' is of type ' + str(type(value)) + ', expected type: ' + str(target_type) + '.')