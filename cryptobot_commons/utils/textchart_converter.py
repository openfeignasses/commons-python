class TextChartConverter:

    def __init__(self, textchart: str):
        self.textchart = textchart

    def to_dicts(self):

        content_list = self.textchart.split('\n')

        lines_data = content_list[1:-1]  # each line of the file
        total_day_number = len(lines_data[0])  # number of values to parse (number of columns)
        max_value = len(lines_data)  # highest possible value (top line of the chart for "BTC"/EUR)

        # For each column, go through each line till we reach a point "X"
        # Store the values of points in a dictionnary
        rows_as_dicts = []
        current_day = 1
        for actual_day_number in range(1, total_day_number - 1):
            actual_line_number = 0
            for line in lines_data:
                if (line[actual_day_number]) == " ":
                    pass
                else:
                    day_value = (max_value - actual_line_number)
                    break
                actual_line_number += 1
            current_day += 1
            row_as_dict = {'time': current_day, "close": day_value}
            rows_as_dicts.append(row_as_dict)

        return rows_as_dicts
