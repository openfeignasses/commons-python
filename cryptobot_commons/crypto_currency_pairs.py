from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.allocation_result import AllocationResult

class CryptoCurrencyPairs:

    def __init__(self, crypto_currency_pairs: list):
        self.pairs = []
        self.pairs_dict = {}
        for crypto_currency_pair in crypto_currency_pairs:
            self.pairs.append(crypto_currency_pair)
            self.pairs_dict[InvestmentUniverse[crypto_currency_pair.name]] = crypto_currency_pair
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        for pair in self.pairs:
            if not isinstance(pair, CryptoCurrencyPair):
                raise (Exception("All elements must be of type CryptoCurrencyPair."))

