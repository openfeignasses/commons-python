import pandas as pd
from cryptobot_commons.signal import Signal
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair

class ConfusionMatrix:
    
    def __init__(self, crypto_currency_pair: CryptoCurrencyPair):
        self.crypto_currency_pair = crypto_currency_pair
        self.matrix = pd.DataFrame(0, index=['LONG_PRED', 'NEUTRAL_PRED', 'SHORT_PRED'], columns=['LONG_TRUE', 'NEUTRAL_TRUE', 'SHORT_TRUE'])
        self._check_confusion_matrix_input(crypto_currency_pair)
    
    def _check_confusion_matrix_input(self, crypto_currency_pair: CryptoCurrencyPair):
        if type(crypto_currency_pair) != CryptoCurrencyPair:
            raise(TypeError("Error: ConfusionMatrix - The crypto_currency_pair must be of type CryptoCurrencyPair."))

    def update(self, predicted_signal:Signal, true_signal:Signal):
        self.matrix.loc[predicted_signal.name + '_PRED', true_signal.name + '_TRUE'] += 1
        
    def __add__(self, other_confusion_matrix):
        self.matrix += other_confusion_matrix.matrix
        return self
