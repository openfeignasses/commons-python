from enum import Enum


class Signal(Enum):
    """
    An Enum that contains all possible outputs for a strategy.
    """

    LONG = 1
    SHORT = 2
    NEUTRAL = 3
