from copy import deepcopy

from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.strategies_results_sorter import StrategiesResultsSorter
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.confusion_matrix_generator import ConfusionMatrixGenerator
from cryptobot_commons.confusion_matrix import ConfusionMatrix

class BookOfConfusionMatrixes:

    def __init__(self, list_strategies_results:list, crypto_currency_pairs:CryptoCurrencyPairs):
        self._list_strategies_results = list_strategies_results
        self._crypto_currency_pairs = crypto_currency_pairs
        self._book_of_confusion_matrixes = []
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        if type(self._list_strategies_results) != list:
            raise(TypeError("Error: The list_strategies_results must be of type list."))
        for result in self._list_strategies_results:
            if type(result) != StrategiesResults:
                raise(TypeError("Error: Each element of the list_strategies_results attribute must be of type StrategiesResults."))
        if type(self._crypto_currency_pairs) != CryptoCurrencyPairs:
            raise(TypeError("Error: The crypto_currency_pairs must be of type CryptoCurrencyPairs."))
        for crypto_currency_pair in self._crypto_currency_pairs.pairs:
            if type(crypto_currency_pair) != CryptoCurrencyPair:
                raise(TypeError("Error: The crypto_currency_pair must be of type CryptoCurrencyPair."))

    def write(self):
        strategies_results_sorted = StrategiesResultsSorter(self._list_strategies_results).sort()
        if len(strategies_results_sorted) != 0:
            for strategies_results in strategies_results_sorted:
                crypto_currency_pair = self._crypto_currency_pairs.pairs_dict[strategies_results.results[0].currency_pair]
                self._book_of_confusion_matrixes.append(ConfusionMatrixGenerator(strategies_results, crypto_currency_pair).generate())
        else:
            for crypto_currency_pair in self._crypto_currency_pairs.pairs:
                self._book_of_confusion_matrixes.append(ConfusionMatrixGenerator(StrategiesResults([]), crypto_currency_pair).generate())

    def read(self):
        return self._book_of_confusion_matrixes

    def get_sum(self) -> ConfusionMatrix:
        """Return the sum of all the confusion matrix in the book of confusion matrix.
        """   
        if len(self._book_of_confusion_matrixes) == 0:
            raise ValueError('The book_of_confusion_matrixes is empty.')
        # Initialization of a 0-value confusion matrix
        confusion_matrix_final = deepcopy(self._book_of_confusion_matrixes[0])
        for col in confusion_matrix_final.matrix.columns:
            confusion_matrix_final.matrix[col].values[:] = 0
        # Then sum all the matrix
        for matrix in self._book_of_confusion_matrixes:
            confusion_matrix_final += matrix
        return confusion_matrix_final
