from .repository import Repository


class StrategiesResultsRepository(Repository):

    def __init__(self, 
                 host_or_none: str = None, 
                 port_or_none: int = None,
                 user_or_none: str = None, 
                 password_or_none: str = None,
                 database_or_none: str = None, 
                 ssl_disabled: bool = False):
        super().__init__(host_or_none, 
                         port_or_none,
                         user_or_none, 
                         password_or_none, 
                         database_or_none, 
                         ssl_disabled)
        self.table_name = "strategies_results"

    def get_last_execution(self):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE execution_id=(SELECT MAX(execution_id) FROM {self.table_name})"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchall()
        cursor.close()

        return out

    def get_limited_ordered(self, limit: int):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE execution_id>=(SELECT MAX(execution_id) FROM {self.table_name}) - {limit} " \
                f"ORDER BY execution_id DESC"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchall()
        cursor.close()

        return out

    def add_result(self, strategy_result: dict):
        query = f"INSERT INTO `cryptobot`.`{self.table_name}` " \
                f"(`strategy_name`, `currency_pair`, `signal`, `execution_id`, `add_date`) " \
                f"VALUES (%s, %s, %s, %s, %s)"
        cursor = self.db.cursor(dictionary=True)

        cursor.execute(query, (strategy_result["strategy_name"],
                               strategy_result["currency_pair"],
                               strategy_result["signal"],
                               strategy_result["execution_id"],
                               strategy_result["add_date"]))
        cursor.close()
        self.save()

        return self

    def get_max_execution_id(self) -> int:
        query = f"SELECT MAX(execution_id) AS max_execution_id FROM {self.table_name}"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchone()
        cursor.close()

        return out["max_execution_id"]

    def get_by_strategy(self, strategy_name):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE strategy_name='{strategy_name}'"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchall()
        cursor.close()

        return out