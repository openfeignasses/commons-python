## Repositories 

### Instanciate a repo

Repositories are classes that enable interaction with the database. This document shows how to use Repositories classes.

Example with stock quotations : 

```python
from repositories import StockQuotationRepository

sq_repo = StockQuotationRepository(
  '{db_host}',
  '{db_user}',
  '{db_password}',
  '{database}'
)
```

It is possible to use environment variables instead of parameters. In this case, pass None to all parameters:
``` python
sq_repo = StockQuotationRepository(
  'None',
  'None',
  'None',
  'None'
)
```

### Methods provided by default

Save changes to the database :

```python
sq_repo.save()
```

**INFO :** Nothing is commited to the distant database before `save` is called.

Basic `SELECT *` query :

```python
output = sq_repo.get_all()
```

Truncate:

```python
sq_repo.truncate()
```