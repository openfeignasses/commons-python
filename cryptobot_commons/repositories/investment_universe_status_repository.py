import warnings
from .repository import Repository


class InvestmentUniverseStatusRepository(Repository):

    def __init__(self, 
                 host_or_none: str = None, 
                 port_or_none: int = None,
                 user_or_none: str = None, 
                 password_or_none: str = None,
                 database_or_none: str = None, 
                 ssl_disabled: bool = False):
        super().__init__(host_or_none, 
                         port_or_none,
                         user_or_none, 
                         password_or_none, 
                         database_or_none, 
                         ssl_disabled)
        self.table_name = "investment_universe_status"

    def add_values(self, currency_pair: str, status: str):
        query = f"INSERT INTO {self.table_name} (currency_pair, status) VALUES (%s, %s)"
        cursor = Repository.db.cursor(dictionary=True)
        cursor.execute(query, (currency_pair, status))
        cursor.close()
        self.save()

        return self

    def update_status(self, currency_pair, new_status):
        query = f"UPDATE `cryptobot`.`{self.table_name}` " \
                f"SET `status`= %s " \
                f"WHERE name=%s"
        cursor = self.db.cursor(dictionary=True)
        cursor.execute(query, [new_status, currency_pair])
        cursor.close()
        self.save()

        if cursor.rowcount == 0:
            warnings.warn("The following statement affected no rows: "+cursor.statement)

        return self

    def get_tradable(self):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE status=%s"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query, ["TRADABLE"])
        out = cursor.fetchall()
        cursor.close()

        return out

    def get_non_tradable(self):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE status=%s"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query, ["NON_TRADABLE"])
        out = cursor.fetchall()
        cursor.close()

        return out

    def get_crashing(self):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE status=%s"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query, ["CRASHING"])
        out = cursor.fetchall()
        cursor.close()

        return out
