from .repository import Repository


class AllocationsResultsRepository(Repository):

    def __init__(self, 
                 host_or_none: str = None, 
                 port_or_none: int = None,
                 user_or_none: str = None, 
                 password_or_none: str = None,
                 database_or_none: str = None, 
                 ssl_disabled: bool = False):
        super().__init__(host_or_none, 
                         port_or_none,
                         user_or_none, 
                         password_or_none, 
                         database_or_none, 
                         ssl_disabled)
        self.table_name = "allocations_results"

    def get_last_execution(self):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE execution_id=(SELECT MAX(execution_id) FROM {self.table_name})"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchall()
        cursor.close()

        return out

    def get_limited_ordered(self, limit: int):
        query = f"SELECT * FROM {self.table_name} " \
                f"WHERE execution_id>=(SELECT MAX(execution_id) FROM {self.table_name}) - {limit} " \
                f"ORDER BY execution_id DESC"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchall()
        cursor.close()

        return out

    def add_result(self, allocation_result: dict):
        query = f"INSERT INTO `cryptobot`.`{self.table_name}` " \
                f"(`currency`, `allocation_name`, `execution_id`, `value_percent`, `add_date`) " \
                f"VALUES (%s, %s, %s, %s, %s)"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query, (allocation_result["currency"],
                               allocation_result["allocation_name"],
                               allocation_result["execution_id"],
                               allocation_result["value_percent"],
                               allocation_result["add_date"]))
        cursor.close()
        self.save()

        return self

    def get_max_execution_id(self) -> int:
        query = f"SELECT MAX(execution_id) AS max_execution_id FROM {self.table_name}"
        cursor = Repository.db.cursor(dictionary=True)

        cursor.execute(query)
        out = cursor.fetchone()
        cursor.close()

        return out["max_execution_id"]


