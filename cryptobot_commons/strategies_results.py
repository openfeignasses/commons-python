from cryptobot_commons.strategy_result import StrategyResult


def check_input(strategies_results):
    if not isinstance(strategies_results, list):
        raise Exception("The parameter strategies_results_raw must be a list.")
    for element in strategies_results:
        if (type(element) != StrategyResult) and (type(element) != dict):
            raise Exception("Each parameter of strategies_results_raw must be a dict or a StrategyResult.")



class StrategiesResults:

    """
    strategies_results: list containing stratgies results in a dict form, or direcltly StrategyResults
    """
    def __init__(self, strategies_results: list):
        check_input(strategies_results)
        self.results = []
        for strategy_result in strategies_results:
            if type(strategy_result) == dict:
                self.results.append(StrategyResult(strategy_result))
            elif type(strategy_result) == StrategyResult:
                self.results.append(strategy_result)
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        for strategy_result in self.results:
            if not isinstance(strategy_result, StrategyResult):
                raise (Exception("All elements of strategies_results must be of type StrategyResult."))
