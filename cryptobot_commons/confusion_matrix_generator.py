from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.confusion_matrix import ConfusionMatrix
from cryptobot_commons.strategy_result_evaluator import StrategyResultEvaluator
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.signal import Signal


class ConfusionMatrixGenerator:

    def __init__(self, strategies_results_for_crypto: StrategiesResults, crypto_currency_pair: CryptoCurrencyPair):
        self.strategies_results_for_crypto = strategies_results_for_crypto
        self.crypto_currency_pair = crypto_currency_pair
        self._check_confusion_matrix_generator_input(strategies_results_for_crypto, crypto_currency_pair)
        
    def _check_confusion_matrix_generator_input(self, strategies_results_for_crypto: StrategiesResults, crypto_currency_pair: CryptoCurrencyPair):
        if type(strategies_results_for_crypto) != StrategiesResults:
            raise(TypeError("Error: ConfusionMatrixGenerator - The strategies_results_for_crypto must be of type StrategiesResults."))
        if type(crypto_currency_pair) != CryptoCurrencyPair:
            raise(TypeError("Error: ConfusionMatrixGenerator - The crypto_currency_pair must be of type CryptoCurrencyPair."))

    def generate(self):
        confusion_matrix = ConfusionMatrix(self.crypto_currency_pair)
        if len(self.strategies_results_for_crypto.results) != 0:
            for strategy_result in self.strategies_results_for_crypto.results:
                true_signal = StrategyResultEvaluator.get_true_signal(strategy_result, self.crypto_currency_pair)
                confusion_matrix.update(strategy_result.signal, true_signal)
        return confusion_matrix