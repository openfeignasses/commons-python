import os
import json
from cryptobot_commons.hyperparameters.hyperparameters import Hyperparameters


class HyperparametersLoader:

    def __init__(self, environment_variable_name: str):
        self.environment_variable_name = environment_variable_name
        self.hyperparameters_dict = None

    def load(self) -> Hyperparameters:
        hyperparameters_dict = json.loads(os.getenv(self.environment_variable_name, None))
        return Hyperparameters(hyperparameters_dict)
