class IntBoundaryConstraint:

    def __init__(self, min_value: int, max_value: int):
        self.__check_input(min_value, max_value)
        self.min_value = min_value
        self.max_value = max_value

    def __check_input(self, min_value: int, max_value: int):
        if type(min_value) is not int or type(max_value) is not int:
            raise Exception("IntConstraints arguments must be of type int.")
        if min_value > max_value:
            raise Exception("IntConstraints min_value is above the max_value.")
        if min_value == max_value:
            raise Exception("IntConstraints min_value is equal to the max_value.")