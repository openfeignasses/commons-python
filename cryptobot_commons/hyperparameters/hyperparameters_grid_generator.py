import numpy as np
import itertools

from cryptobot_commons.hyperparameters.hyperparameters import Hyperparameters
from cryptobot_commons.hyperparameters.hyperparameters_constraints import HyperparametersConstraints
from cryptobot_commons.hyperparameters.boolean_boundary_constraint import BooleanBoundaryConstraint
from cryptobot_commons.hyperparameters.float_boundary_constraint import FloatBoundaryConstraint
from cryptobot_commons.hyperparameters.int_boundary_constraint import IntBoundaryConstraint
from cryptobot_commons.hyperparameters.string_boundary_constraint import StringBoundaryConstraint
from cryptobot_commons.hyperparameters.is_above_functional_constraint import IsAboveFunctionalConstraint


class HyperparametersGridGenerator:

    def generate_grid(self, hyperparameters_boundary_constraints: HyperparametersConstraints, hyperparameters_functional_constraints: HyperparametersConstraints, grid_config: dict):
        self.__check_inputs(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        grid = self.__build_grid(hyperparameters_boundary_constraints, grid_config)
        grid = self.__filter_grid_with_constraints(grid, hyperparameters_functional_constraints)
        return grid

    def __check_inputs(self, hyperparameters_boundary_constraints: HyperparametersConstraints, hyperparameters_functional_constraints: HyperparametersConstraints, grid_config: dict):
        if not isinstance(hyperparameters_boundary_constraints, HyperparametersConstraints):
            raise TypeError("hyperparameters_boundary_constraintsError: hyperparameters_boundary_constraints must be of type HyperparametersConstraints.")
        if (not isinstance(hyperparameters_functional_constraints, HyperparametersConstraints)) and (not hyperparameters_functional_constraints is None):
            raise TypeError("Error: hyperparameters_functional_constraints must be of type HyperparametersConstraints or None.")
        if not isinstance(grid_config, dict):
            raise TypeError("Error: grid_config must be of type dict.")

    def __build_grid(self, hyperparameters_boundary_constraints: HyperparametersConstraints, grid_config: dict):
        dict_of_possibilities = self.__build_dict_of_possibilities(hyperparameters_boundary_constraints, grid_config)
        list_possibilities = list(dict_of_possibilities.values())
        all_param_combination = list(itertools.product(*list_possibilities))
        list_hyperparameters = self.__generate_hyperparameters_list(all_param_combination, hyperparameters_boundary_constraints)
        return list_hyperparameters

    def __generate_hyperparameters_list(self, all_param_combination, hyperparameters_boundary_constraints):
        list_hyperparameters = []
        for combi in all_param_combination:
            hyperparameters_raw = {}
            i = 0
            for param in hyperparameters_boundary_constraints.constraints_mapping.keys():
                hyperparameters_raw[param] = combi[i]
                i += 1
            list_hyperparameters.append(Hyperparameters(hyperparameters_raw))
        return list_hyperparameters

    def __build_dict_of_possibilities(self, hyperparameters_boundary_constraints, grid_config):
        dict_of_possibilities = {}
        for constraint_name in hyperparameters_boundary_constraints.constraints_mapping.keys():
            self.__check_constraint_and_grid_config(constraint_name, hyperparameters_boundary_constraints, grid_config)
            if isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], IntBoundaryConstraint):
                dict_of_possibilities[constraint_name] = self.__generate_int_possibilities(constraint_name, hyperparameters_boundary_constraints.constraints_mapping[constraint_name], grid_config)
            elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], FloatBoundaryConstraint):
                dict_of_possibilities[constraint_name] = self.__generate_float_possibilities(constraint_name, hyperparameters_boundary_constraints.constraints_mapping[constraint_name], grid_config)
            elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], StringBoundaryConstraint):
                dict_of_possibilities[constraint_name] = hyperparameters_boundary_constraints.constraints_mapping[constraint_name].possible_values
            elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], BooleanBoundaryConstraint):
                dict_of_possibilities[constraint_name] = [True, False]
            else:
                raise TypeError("Error: the type of the boundary constraints is not IntBoundaryConstraint, or FloatBoundaryConstraint, or StringBoundaryConstraint, or BooleanBoundaryConstraint")
        return dict_of_possibilities
    
    def __check_constraint_and_grid_config(self, constraint_name: str, hyperparameters_boundary_constraints: HyperparametersConstraints, grid_config: dict):
        # Nothing to configure in grid_config for Int and String boundary constraints
        if isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], BooleanBoundaryConstraint):
            return
        elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], StringBoundaryConstraint):
            return
        # Require configuration in grid_config for other boundary constraints types
        elif not constraint_name in grid_config.keys():
            raise Exception("Error: you must specify a configuration in grid_config for all boundary constraints exept for Boolean and String ones.")
        # Allow 'no_discretisation' configuration only for Int boundary constrains
        elif not isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], IntBoundaryConstraint) and (grid_config[constraint_name] == 'no_discretisation'):
            raise TypeError("Error: grid_config can be 'no_discretisation' only if the associated boundary constraints are IntBoundaryConstraint.")
        # Do not allow less than 2 discretisations
        elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], IntBoundaryConstraint) and (grid_config[constraint_name] != 'no_discretisation'):
            nb_discretisations = grid_config[constraint_name]
            if nb_discretisations < 2:
                raise TypeError("Error: the number of discretisations specified in grid_config must be greater or equal to 2.")
        elif isinstance(hyperparameters_boundary_constraints.constraints_mapping[constraint_name], FloatBoundaryConstraint):
            nb_discretisations = grid_config[constraint_name]
            if nb_discretisations < 2:
                raise TypeError("Error: the number of discretisations specified in grid_config must be greater or equal to 2.")


    def __generate_int_possibilities(self, constraint_name, int_boundary_constraint, grid_config):
        possibilities = []
        if grid_config[constraint_name] == 'no_discretisation':
            return list(range(int_boundary_constraint.min_value, int_boundary_constraint.max_value+1))
        else:
            nb_discretisation = grid_config[constraint_name]
            possibilities = [int_boundary_constraint.min_value + ((int_boundary_constraint.max_value-int_boundary_constraint.min_value)/(nb_discretisation-1))*i for i in range(nb_discretisation)]
            possibilities = [int(poss) for poss in possibilities]
            return possibilities

    def __generate_float_possibilities(self, constraint_name, float_boundary_constraint, grid_config):
        nb_discretisation = grid_config[constraint_name] 
        possibilities = [float_boundary_constraint.min_value + ((float_boundary_constraint.max_value-float_boundary_constraint.min_value)/(nb_discretisation-1))*i for i in range(nb_discretisation)]
        possibilities = [round(poss, 6) for poss in possibilities]
        return possibilities

    def __filter_grid_with_constraints(self, grid: list, hyperparameters_functional_constraints: HyperparametersConstraints):
        if hyperparameters_functional_constraints is None:
            return grid
        
        final_grid = []
        for hyperparameters in grid:
            is_valid = self.__is_hyperparameter_valid(hyperparameters_functional_constraints, hyperparameters)
            if is_valid:
                final_grid.append(hyperparameters)
        return final_grid 

    def __is_hyperparameter_valid(self, hyperparameters_functional_constraints: HyperparametersConstraints, hyperparameters: Hyperparameters):
        is_valid  = True
        for constraint_name in hyperparameters_functional_constraints.constraints_mapping.keys():
            if isinstance(hyperparameters_functional_constraints.constraints_mapping[constraint_name], IsAboveFunctionalConstraint):
                value_above = hyperparameters.entries[hyperparameters_functional_constraints.constraints_mapping[constraint_name].value_above]
                value_below = hyperparameters.entries[hyperparameters_functional_constraints.constraints_mapping[constraint_name].value_below]
                if not value_above > value_below:
                    is_valid = False
        return is_valid

