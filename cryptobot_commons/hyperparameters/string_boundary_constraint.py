class StringBoundaryConstraint:

    def __init__(self, possible_values: list):
        self.__check_input(possible_values)
        self.possible_values = possible_values

    def __check_input(self, possible_values: list):
        if type(possible_values) is not list:
            raise Exception("StringConstraint argument should be a non-empty list of str values.")
        if len(possible_values) == 0:
            raise Exception("StringConstraint argument should be a non-empty list of str values.")
        for value in possible_values:
            if type(value) is not str:
                raise Exception("StringConstraint argument should be a non-empty list of str values.")