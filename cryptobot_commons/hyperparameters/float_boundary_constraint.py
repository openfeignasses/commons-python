class FloatBoundaryConstraint:

    def __init__(self, min_value: float, max_value: float):
        self.__check_input(min_value, max_value)
        self.min_value = min_value
        self.max_value = max_value

    def __check_input(self, min_value: float, max_value: float):
        if type(min_value) is not float or type(max_value) is not float:
            raise Exception("FloatBoundaryConstraint arguments must be of type float.")
        if min_value > max_value:
            raise Exception("FloatBoundaryConstraint min_value is above the max_value.")
        if min_value == max_value:
            raise Exception("FloatBoundaryConstraint min_value is equal to the max_value.")