class Hyperparameters:

    def __init__(self, hyperparameters_dict: dict):
        self.entries = hyperparameters_dict
        self.__check_initialized_dict()

    def __eq__(self, other):
        return self.entries == other.entries

    def __check_initialized_dict(self):
        self.__check_entries_type()
        self.__check_entries_emptyness()
        for entry in self.entries:
            self.__check_values_emptyness(entry)
            self.__check_values_types(entry)
            if type(self.entries[entry]) is list:
                self.__check_list_types(entry)

    def __check_entries_type(self):
        if not isinstance(self.entries, dict):
            raise Exception("Error: loaded hyperparameters must be wrapped in a dict.")

    def __check_entries_emptyness(self):
        if len(self.entries) == 0:
            raise Exception("Error: the loaded hyperparameters dict cannot be empty.")

    def __check_values_emptyness(self, entry):
        if self.entries[entry] is None:
            raise Exception("Error: the loaded hyperparameters dict cannot contains None values.")

    def __check_values_types(self, entry):
        if type(self.entries[entry]) is not float and \
                type(self.entries[entry]) is not int and \
                type(self.entries[entry]) is not bool and \
                type(self.entries[entry]) is not str and \
                type(self.entries[entry]) is not list:
            raise Exception("Error: hyperparameters values must be float, int, bool, str or list.")

    def __check_list_types(self, entry: list):
        list_to_check = self.entries[entry]
        if len(list_to_check) == 0:
            raise Exception("Error: a hyperparameters list cannot be empty.")
        for item in list_to_check:
            if type(item) is not str:
                raise Exception("Error: a hyperparameter list can only contain items of type str.")
