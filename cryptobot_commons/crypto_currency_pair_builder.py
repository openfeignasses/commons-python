import pandas as pd

from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair


class CryptoCurrencyPairBuilder():

    def __init__(self, name: str, stock_quotations_for_currency_raw: list):
        self.name = name
        self.stock_quotations_for_currency_raw = stock_quotations_for_currency_raw

    def build(self):

        self.dataframe = pd.DataFrame(
            data=self.stock_quotations_for_currency_raw,
            columns=["time", "open", "high", "low", "close", "vwap", "volume", "count"]
        ).set_index("time")

        self.check_dataframe_completeness()
        self.convert_all_to_float_type()

        return CryptoCurrencyPair(self.name, self.dataframe)

    def check_dataframe_completeness(self):
        if not "time" == self.dataframe.index.name:
            raise Exception("time must be the index of the Dataframe")
        if not "open" == self.dataframe.keys()[0]:
            raise Exception("open must be one of the keys of the Dataframe")
        if not "high" == self.dataframe.keys()[1]:
            raise Exception("high must be one of the keys of the Dataframe")
        if not "low" == self.dataframe.keys()[2]:
            raise Exception("low must be one of the keys of the Dataframe")
        if not "close" == self.dataframe.keys()[3]:
            raise Exception("close must be one of the keys of the Dataframe")
        if not "vwap" == self.dataframe.keys()[4]:
            raise Exception("vwap must be one of the keys of the Dataframe")
        if not "volume" == self.dataframe.keys()[5]:
            raise Exception("volume must be one of the keys of the Dataframe")
        if not "count" == self.dataframe.keys()[6]:
            raise Exception("count must be one of the keys of the Dataframe")
        if not len(self.dataframe.index) > 0:
            raise Exception("index length must be greater than 0")
        if not len(self.dataframe["close"]) > 0:
            raise Exception("close column length must be greater than 0")

    def convert_all_to_float_type(self):
        self.dataframe = self.dataframe.astype(float)
