from datetime import datetime
import pandas as pd
import numpy as np
import copy

from cryptobot_commons.time_series import TimeSeries


def check_inputs(name: str, data: pd.Series):
    if not isinstance(name, str):
        raise (Exception("The CryptoCurrencyPair name parameter must be a string."))
    if not isinstance(data, pd.core.frame.DataFrame):
        raise (Exception("The CryptoCurrencyPair data must be a pd.core.frame.DataFrame."))
    # If the index is int (timestamp), it will be convert into datetime in the CryptoCurrencyPair constructor
    if not isinstance(data.index[0], int) and not isinstance(data.index[0], np.int64) and not isinstance(data.index[0], datetime):
        raise (Exception("The CryptoCurrencyPair data index must be of type int or datetime."))


class CryptoCurrencyPair:
    """
    This class models a cryptocurrency pair
    inputs:
        * name: Name of the cryptocurrency pair (Cryptobot format)
        * data: A DataFrame with OHLC, vwap, volume and count data 
                associated to the cryptocurrency pair.
                The index has to be int (timestamp) or datetime.
                If the index is timestamp, it will be converted into datetime.
    """

    def __init__(self, name: str, data: pd.Series):
        data = copy.deepcopy(data) # because we can have problems of reference

        check_inputs(name, data)
        
        data['time'] = data.index
        data['time'] = data['time'].apply(datetime.fromtimestamp)
        data = data.set_index('time')

        self.name = name
        self.open = TimeSeries(name, data["open"])
        self.high = TimeSeries(name, data["high"])
        self.low = TimeSeries(name, data["low"])
        self.close = TimeSeries(name, data["close"])
        self.vwap = TimeSeries(name, data["vwap"])
        self.volume = TimeSeries(name, data["volume"])
        self.count = TimeSeries(name, data["count"])
        del data

    def set_quantity_of_data(self, number_of_observations: int, end_of_series=None):
        """This function allows to choose the number of data to keep in the TimeSeries in the CryptoCurrencyPair.
        Moreover, it allows also to choose the id of the end of the series (since today).
        For instance, if end_of_series = 1000, it will delete the 1000 last data.
        It can be useful for the calibration. 

        Args:
            number_of_observations (int): Number of observations to keep in the Series.
            end_of_series ([type], optional): Number of data to delete at the end of the Series. Defaults to None.

        Raises:
            ValueError: if not enough observations in the cryptocurrency pair.
        """
        if number_of_observations > len(self.close.data):
            raise ValueError("Not enough data for " + str(number_of_observations) + " observations to keep.")
        if end_of_series != None:
            self.open.data = self.open.data.iloc[:((-1)*end_of_series)]
            self.high.data = self.high.data.iloc[:((-1)*end_of_series)]
            self.low.data = self.low.data.iloc[:((-1)*end_of_series)]
            self.close.data = self.close.data.iloc[:((-1)*end_of_series)]
            self.vwap.data = self.vwap.data.iloc[:((-1)*end_of_series)]
            self.volume.data = self.volume.data.iloc[:((-1)*end_of_series)]
            self.count.data = self.count.data.iloc[:((-1)*end_of_series)]
        self.open.data = self.open.data.iloc[((-1)*number_of_observations):]
        self.high.data = self.high.data.iloc[((-1)*number_of_observations):]
        self.low.data = self.low.data.iloc[((-1)*number_of_observations):]
        self.close.data = self.close.data.iloc[((-1)*number_of_observations):]
        self.vwap.data = self.vwap.data.iloc[((-1)*number_of_observations):]
        self.volume.data = self.volume.data.iloc[((-1)*number_of_observations):]
        self.count.data = self.count.data.iloc[((-1)*number_of_observations):]