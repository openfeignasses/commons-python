from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus
from cryptobot_commons.repositories import InvestmentUniverseStatusRepository


class InvestmentUniverseStatusSaver:

    def __init__(self, investment_universe_status: InvestmentUniverseStatus):
        if not isinstance(investment_universe_status, InvestmentUniverseStatus):
            raise Exception("The investment_universe_status parameter must be of type InvestmentUniverseStatus.")

        self.repository = InvestmentUniverseStatusRepository()
        self.investment_universe_status = investment_universe_status

    def save(self):
        for currency_pair, new_status in self.investment_universe_status.status.items():
            self.repository.update_status(currency_pair.name, new_status)

