from distutils.core import setup

setup(
    name='cryptobot-commons',
    version='2.4.0',
    description='Cryptobot\'s Shared Library',
    author='Les Feignasses',
    packages=[
        'cryptobot_commons',
        'cryptobot_commons/repositories',
        'cryptobot_commons/retrievers',
        'cryptobot_commons/savers',
        'cryptobot_commons/hyperparameters',
        'cryptobot_commons/utils',
        'cryptobot_commons.technical_indicators'
    ],
    install_requires=[
        "mysql-connector-python==8.0.18",
    ],
)