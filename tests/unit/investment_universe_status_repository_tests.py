import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import InvestmentUniverseStatusRepository


class InvestmentUniverseStatusRepositoryTestCase(unittest.TestCase):

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_get_all(self, mysql_mock):
        investment_universe_status_repository = InvestmentUniverseStatusRepository()
        investment_universe_status_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "name": "BTC_EUR",
            "status": "TRADABLE",
        }, {
            "name": "GNO_EUR",
            "status": "TRADABLE",
        }])
        result = investment_universe_status_repository.get_all()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_get_tradable(self, mysqk_mock):
        investment_universe_status_repository = InvestmentUniverseStatusRepository()
        investment_universe_status_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "name": "BTC_EUR",
            "status": "TRADABLE",
        }, {
            "name": "GNO_EUR",
            "status": "TRADABLE",
        }])
        result = investment_universe_status_repository.get_tradable()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_get_non_tradable(self, mysqk_mock):
        investment_universe_status_repository = InvestmentUniverseStatusRepository()
        investment_universe_status_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "name": "BTC_EUR",
            "status": "NON_TRADABLE",
        }, {
            "name": "GNO_EUR",
            "status": "NON_TRADABLE",
        }])
        result = investment_universe_status_repository.get_non_tradable()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)
