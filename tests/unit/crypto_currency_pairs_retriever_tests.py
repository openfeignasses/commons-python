import unittest
import pandas as pd
import datetime
from unittest.mock import MagicMock, patch
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs
from cryptobot_commons.repositories import StockQuotationsRepository
from cryptobot_commons.retrievers.crypto_currency_pairs_retriever import CryptoCurrencyPairsRetriever
from cryptobot_commons.time_series import TimeSeries


class CryptoCurrencyPairsRetrieverTestCase(unittest.TestCase):

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    @patch.object(StockQuotationsRepository, 'get_by_currency', MagicMock(return_value=[{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1600,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1601,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1602,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234569
    }]))
    def test_retrieve(self, mysql_mock):
        self.crypto_currency_pairs_retriever = CryptoCurrencyPairsRetriever("BY_CURRENCY", "BTC_EUR")
        retrieved_crypto_currency_pairs = self.crypto_currency_pairs_retriever.execute()

        self.assertIsInstance(retrieved_crypto_currency_pairs, CryptoCurrencyPairs)
        self.assertIsInstance(retrieved_crypto_currency_pairs.pairs, list)
        self.assertTrue(len(retrieved_crypto_currency_pairs.pairs) == 1)
        self.assertIsInstance(retrieved_crypto_currency_pairs.pairs[0], CryptoCurrencyPair)

    @patch('mysql.connector.connect')
    @patch.object(StockQuotationsRepository, 'get_distinct_currency_pairs', MagicMock(return_value=[{
        "currency_pair": "BTC_EUR",
    }, {
        "currency_pair": "ETH_EUR",
    }]))
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    @patch.object(StockQuotationsRepository, 'get_by_currency', MagicMock(side_effect=[[{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1600,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1601,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }], [{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 52,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 54,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }]]))
    def test_retrieve_all(self, mysql_mock):
        self.crypto_currency_pairs_retriever = CryptoCurrencyPairsRetriever("ALL", name=None)
        retrieved_crypto_currency_pairs = self.crypto_currency_pairs_retriever.execute()

        self.assertIsInstance(retrieved_crypto_currency_pairs, CryptoCurrencyPairs)
        self.assertIsInstance(retrieved_crypto_currency_pairs.pairs, list)
        self.assertTrue(len(retrieved_crypto_currency_pairs.pairs) == 2)
        self.assertIsInstance(retrieved_crypto_currency_pairs.pairs[0], CryptoCurrencyPair)
        self.assertTrue(retrieved_crypto_currency_pairs.pairs[0].name == "BTC_EUR")
        self.assertIsInstance(retrieved_crypto_currency_pairs.pairs[1], CryptoCurrencyPair)
        self.assertTrue(retrieved_crypto_currency_pairs.pairs[1].name == "ETH_EUR")

    @patch('mysql.connector.connect')
    @patch.object(StockQuotationsRepository, 'get_by_currency', MagicMock(return_value=[{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1600,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1601,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }]))
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_content(self, mysql_mock):
        self.crypto_currency_pairs_retriever = CryptoCurrencyPairsRetriever("BY_CURRENCY", "BTC_EUR")
        retrieved_crypto_currency_pairs = self.crypto_currency_pairs_retriever.execute()
        currency_pair = retrieved_crypto_currency_pairs.pairs[0]

        self.assertIsInstance(currency_pair.close, TimeSeries)
        self.assertTrue(currency_pair.close.name == "BTC_EUR")
        self.assertIsInstance(currency_pair.close.data, pd.Series)
        self.assertTrue(currency_pair.close.data.index[0] == datetime.datetime(1970, 1, 15, 6, 56, 7))
        self.assertTrue(currency_pair.close.data.index[1] == datetime.datetime(1970, 1, 15, 6, 56, 8))
        self.assertTrue(currency_pair.close.data.values[0] == 1600)
        self.assertTrue(currency_pair.close.data.values[1] == 1601)

    @patch('mysql.connector.connect')
    @patch.object(StockQuotationsRepository, 'get_distinct_currency_pairs', MagicMock(return_value=[{
        "currency_pair": "BTC_EUR",
    }, {
        "currency_pair": "ETH_EUR",
    }]))
    @patch.object(StockQuotationsRepository, 'get_by_currency_last_hours', MagicMock(return_value=[{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1600,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1601,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }]))
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_last_100_hours(self, mysql_mock):
        self.crypto_currency_pairs_retriever = CryptoCurrencyPairsRetriever("LAST_HOURS", name=None, limit=100)
        retrieved_crypto_currency_pairs = self.crypto_currency_pairs_retriever.execute()
        currency_pair = retrieved_crypto_currency_pairs.pairs[0]

        self.assertIsInstance(currency_pair.close, TimeSeries)
        self.assertTrue(currency_pair.close.name == "BTC_EUR")
        self.assertIsInstance(currency_pair.close.data, pd.Series)
        self.assertTrue(currency_pair.close.data.index[0] == datetime.datetime(1970, 1, 15, 6, 56, 7))
        self.assertTrue(currency_pair.close.data.index[1] == datetime.datetime(1970, 1, 15, 6, 56, 8))
        self.assertTrue(currency_pair.close.data.values[0] == 1600)
        self.assertTrue(currency_pair.close.data.values[1] == 1601)
    
    @patch('mysql.connector.connect')
    @patch.object(StockQuotationsRepository, 'get_distinct_currency_pairs', MagicMock(return_value=[{
        "currency_pair": "BTC_EUR",
    }, {
        "currency_pair": "ETH_EUR",
    }]))
    @patch.object(StockQuotationsRepository, 'get_by_currency_last_hours', MagicMock(return_value=[{
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1600,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234567
    }, {
        "open": 0.0,
        "high": 0.0,
        "low": 0.0,
        "close": 1601,
        "vwap": 0.0,
        "volume": 0.0,
        "count": 0.0,
        "time": 1234568
    }]))
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_last_hours_default_limit(self, mysql_mock):
        self.crypto_currency_pairs_retriever = CryptoCurrencyPairsRetriever("LAST_HOURS", name=None)
        retrieved_crypto_currency_pairs = self.crypto_currency_pairs_retriever.execute()
        currency_pair = retrieved_crypto_currency_pairs.pairs[0]

        self.assertIsInstance(currency_pair.close, TimeSeries)
        self.assertTrue(currency_pair.close.name == "BTC_EUR")
        self.assertIsInstance(currency_pair.close.data, pd.Series)
        self.assertTrue(currency_pair.close.data.index[0] == datetime.datetime(1970, 1, 15, 6, 56, 7))
        self.assertTrue(currency_pair.close.data.index[1] == datetime.datetime(1970, 1, 15, 6, 56, 8))
        self.assertTrue(currency_pair.close.data.values[0] == 1600)
        self.assertTrue(currency_pair.close.data.values[1] == 1601)
        
    def test_retrieve_fails(self):
        with self.assertRaises(Exception):
            CryptoCurrencyPairsRetriever("INEXISTANT_MODE", name=None)
