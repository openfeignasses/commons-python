import unittest
from cryptobot_commons.signal import Signal


class SignalTestCase(unittest.TestCase):

    def test_signal_count(self):
        # Useful to test uniqueness of values. A non unique key will throw an error.
        self.assertTrue(len(Signal) == 3)

    def test_signal_long(self):
        self.assertTrue(Signal.LONG is not None)

    def test_signal_short(self):
        self.assertTrue(Signal.SHORT is not None)

    def test_signal_neutral(self):
        self.assertTrue(Signal.NEUTRAL is not None)

    def test_string_convertion(self):
        self.assertTrue(str(Signal.LONG.name) == "LONG")
