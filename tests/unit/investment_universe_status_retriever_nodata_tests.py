import unittest
from unittest.mock import MagicMock, patch

from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus
from cryptobot_commons.repositories import InvestmentUniverseStatusRepository
from cryptobot_commons.retrievers import InvestmentUniverseStatusRetriever


class InvestmentUniverseRetrieverTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.investment_universe_status_repository = InvestmentUniverseStatusRepository()
        cls.investment_universe_status_raw = []
        cls.investment_universe_status_repository.db.cursor().fetchall = MagicMock(
            return_value=cls.investment_universe_status_raw)
        cls.investment_universe_status = InvestmentUniverseStatusRetriever("ALL")

    def test_retrieve(self):
        retrieved_investment_universe_status = self.investment_universe_status.execute()
        self.assertIsInstance(retrieved_investment_universe_status, InvestmentUniverseStatus)
