import unittest
import datetime
import pandas as pd

from cryptobot_commons.strategy_result_evaluator import StrategyResultEvaluator
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.signal import Signal
from cryptobot_commons.strategy_result import StrategyResult
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair


class StrategyResultEvaluatorTestCase(unittest.TestCase):

    def setUp(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        dummy_values = [0]*len(quotes_list)
        timestamp_list = [1588618800 + 3600 * i for i in range(len(quotes_list))]
        self.df = pd.DataFrame(index=timestamp_list)
        self.df["open"] = dummy_values
        self.df["high"] = dummy_values
        self.df["low"] = dummy_values
        self.df["close"] = quotes_list
        self.df["vwap"] = dummy_values
        self.df["volume"] = dummy_values
        self.df["count"] = dummy_values
        self.crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=self.df)

    def test_exception_if_add_date_before_dataset_min_date(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2019, 5, 5, 0, 0),
            "execution_id": 1
        })
        with self.assertRaises(Exception):
            evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                    crypto_currency_pair=self.crypto_pair)

    def test_exception_if_add_date_after_dataset_max_date(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2021, 5, 5, 0, 0),
            "execution_id": 1
        })
        with self.assertRaises(Exception):
            evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                    crypto_currency_pair=self.crypto_pair)

    def test_wrong_input_1(self):
        with self.assertRaises(Exception):
            StrategyResultEvaluator.evaluate_the_signal(strategy_result=[], crypto_currency_pair=[])

    def test_wrong_input_2(self):
        with self.assertRaises(Exception):
            StrategyResultEvaluator.evaluate_the_signal({})

    def test_evaluate_1_1(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2020, 5, 5, 9, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == False

    def test_evaluate_1_2(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": datetime.datetime(2020, 5, 5, 9, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == False

    def test_evaluate_1_3(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": datetime.datetime(2020, 5, 5, 9, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == True

    def test_evaluate_2_1(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": datetime.datetime(2020, 5, 5, 10, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == True

    def test_evaluate_2_2(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2020, 5, 5, 10, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == False

    def test_evaluate_2_3(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": datetime.datetime(2020, 5, 5, 10, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.0
        assert evaluation == False

    def test_evaluate_3_1(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": datetime.datetime(2020, 5, 5, 5, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.25
        assert evaluation == False

    def test_evaluate_3_2(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2020, 5, 5, 5, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.25
        assert evaluation == True

    def test_evaluate_3_3(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": datetime.datetime(2020, 5, 5, 5, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: 0.25
        assert evaluation == False

    def test_evaluate_4_1(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": datetime.datetime(2020, 5, 5, 11, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.2
        assert evaluation == False

    def test_evaluate_4_2(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2020, 5, 5, 11, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.2
        assert evaluation == False

    def test_evaluate_4_3(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": datetime.datetime(2020, 5, 5, 11, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.2
        assert evaluation == True

    def test_evaluate_5_1(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": datetime.datetime(2020, 5, 5, 12, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.363636363636364
        assert evaluation == False

    def test_evaluate_5_2(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": datetime.datetime(2020, 5, 5, 12, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.363636363636364
        assert evaluation == True

    def test_evaluate_5_3(self):
        strategy_result = StrategyResult({
            "strategy_name": "SMA_CROSSOVERS",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": datetime.datetime(2020, 5, 5, 12, 5),
            "execution_id": 1
        })
        evaluation = StrategyResultEvaluator.evaluate_the_signal(strategy_result=strategy_result,
                                                                 crypto_currency_pair=self.crypto_pair)
        # return expected: -0.363636363636364
        assert evaluation == False