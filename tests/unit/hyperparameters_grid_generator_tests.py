import unittest
import pandas as pd
import traceback

from cryptobot_commons.hyperparameters.boolean_boundary_constraint import BooleanBoundaryConstraint
from cryptobot_commons.hyperparameters.float_boundary_constraint import FloatBoundaryConstraint
from cryptobot_commons.hyperparameters.int_boundary_constraint import IntBoundaryConstraint
from cryptobot_commons.hyperparameters.string_boundary_constraint import StringBoundaryConstraint
from cryptobot_commons.hyperparameters.is_above_functional_constraint import IsAboveFunctionalConstraint
from cryptobot_commons.hyperparameters.hyperparameters_constraints import HyperparametersConstraints
from cryptobot_commons.hyperparameters.hyperparameters import Hyperparameters
from cryptobot_commons.hyperparameters.hyperparameters_grid_generator import HyperparametersGridGenerator
from cryptobot_commons.hyperparameters.hyperparameters_constraints import HyperparametersConstraints


def load_a_scenario(scenario_id: str, types_hyperparameters: dict):
    df_scenario = pd.read_csv('tests/resources/scenarios_generation_hyperparameters_grid/' + str(scenario_id) +'.csv', sep=';')
    id_hyperparameters = list(df_scenario.columns)
    list_hyperparameters = list()
    for row in df_scenario.index:
        dict_hyperparameters = {}
        for param in id_hyperparameters:
            dict_hyperparameters[param] = convert_parameter(param, df_scenario.loc[row, param], types_hyperparameters)
        list_hyperparameters.append(Hyperparameters(dict_hyperparameters))
    return list_hyperparameters


def convert_parameter(hyperparam_name: str, param_value, types_hyperparameters: dict):
    # To be sure of the type (useful when bool):
    param_value = str(param_value)

    if types_hyperparameters[hyperparam_name] == 'string':
        return param_value
    elif types_hyperparameters[hyperparam_name] == 'int':
        return int(param_value)
    elif types_hyperparameters[hyperparam_name] == 'float':
        return float(param_value)
    elif (types_hyperparameters[hyperparam_name] == 'bool') and (param_value=='True'):
        return True
    elif (types_hyperparameters[hyperparam_name] == 'bool') and (param_value=='False'):
        return False

class HyperparametersGridGeneratorTestCase(unittest.TestCase):
    """
    Cf commons-python/tests/resources/tests_generation_hyperparameter_grid.ods
    for scenarios details and explanations
    """

    #----------------------------------------------------------------
    # Happy Paths

    def test_scenario_1(self):
        types_hyperparameters = {
            'short_period': 'int',
            'long_period': 'int'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario1', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "short_period": IntBoundaryConstraint(3, 5),
                                                                    "long_period": IntBoundaryConstraint(4, 7)
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint("long_period", "short_period")
                                                                })
        grid_config = {
            'short_period': 'no_discretisation',
            'long_period': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)
        

    def test_scenario_2(self):
        types_hyperparameters = {
            'short_period': 'int',
            'long_period': 'int',
            'a_bool_param': 'bool'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario2', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "short_period": IntBoundaryConstraint(3, 4),
                                                                    "long_period": IntBoundaryConstraint(4, 6),
                                                                    "a_bool_param": BooleanBoundaryConstraint()
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint("long_period", "short_period")
                                                                })
        grid_config = {
            'short_period': 'no_discretisation',
            'long_period': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)

    def test_scenario_3(self):
        types_hyperparameters = {
            'learning_rate_a': 'float',
            'learning_rate_b': 'float'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario3', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "learning_rate_b": FloatBoundaryConstraint(min_value=0.01, max_value=0.1)
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint(id_hyperparameter_above='learning_rate_a', id_hyperparameter_below='learning_rate_b')
                                                                })
        grid_config = {
            'learning_rate_a': 7,
            'learning_rate_b': 7
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)

    def test_scenario_4(self):
        types_hyperparameters = {
            'learning_rate_a': 'float',
            'learning_rate_b': 'float'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario4', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "learning_rate_b": FloatBoundaryConstraint(min_value=0.01, max_value=0.1)
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'learning_rate_a': 4,
            'learning_rate_b': 4
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)

    def test_scenario_5(self):
        types_hyperparameters = {
            'nb_neurons_per_layer': 'int',
            'nb__hidden_layers': 'int'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario5', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "nb_neurons_per_layer": IntBoundaryConstraint(min_value=28, max_value=512),
                                                                    "nb__hidden_layers": IntBoundaryConstraint(min_value=1, max_value=4)
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'nb_neurons_per_layer': 4,
            'nb__hidden_layers': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)

    def test_scenario_6(self):
        types_hyperparameters = {
            'nb_neurons_per_layer': 'int',
            'nb__hidden_layers': 'int',
            'activation_function': 'string'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario6', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "nb_neurons_per_layer": IntBoundaryConstraint(min_value=28, max_value=512),
                                                                    "nb__hidden_layers": IntBoundaryConstraint(min_value=1, max_value=3),
                                                                    "activation_function": StringBoundaryConstraint(['relu', 'leaky_relu', 'elu'])
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'nb_neurons_per_layer': 4,
            'nb__hidden_layers': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)

    def test_scenario_7(self):
        types_hyperparameters = {
            'nb_neurons_per_layer': 'int',
            'nb__hidden_layers': 'int',
            'is_resnet': 'bool'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario7', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "nb_neurons_per_layer": IntBoundaryConstraint(min_value=28, max_value=512),
                                                                    "nb__hidden_layers": IntBoundaryConstraint(min_value=1, max_value=3),
                                                                    "is_resnet": BooleanBoundaryConstraint()
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'nb_neurons_per_layer': 4,
            'nb__hidden_layers': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)



    def test_scenario_8(self):
        types_hyperparameters = {
            'a_bool_param': 'bool'
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario8', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "a_bool_param": BooleanBoundaryConstraint()
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {}
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)


    def test_scenario_9(self):
        types_hyperparameters = {
            'learning_rate_a': 'float',
            'period': 'int',
        }
        expected_list_of_hypeparameters = load_a_scenario(scenario_id='scenario9', types_hyperparameters=types_hyperparameters)

        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "period": IntBoundaryConstraint(min_value=1, max_value=2)
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'learning_rate_a': 2,
            'period': 'no_discretisation'
        }
        proposed_list_of_hyperparameters = HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
        self.assertListEqual(expected_list_of_hypeparameters, proposed_list_of_hyperparameters)


    #----------------------------------------------------------------
    # Unhappy Paths

    def test_forget_grid_configuration_1(self):
        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "short_period": IntBoundaryConstraint(3, 5),
                                                                    "long_period": IntBoundaryConstraint(4, 7)
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint("long_period", "short_period")
                                                                })
        grid_config = {
            'long_period': 'no_discretisation',
        }

        with self.assertRaises(Exception):
            HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)

    def test_forget_grid_configuration_2(self):
        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "learning_rate_b": FloatBoundaryConstraint(min_value=0.01, max_value=0.1)
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint(id_hyperparameter_above='learning_rate_a', id_hyperparameter_below='learning_rate_b')
                                                                })
        grid_config = {
            'learning_rate_a': 'no_discretisation'
        }

        with self.assertRaises(Exception):
            HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)

    def test_try_no_discretisation_on_float(self):
        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "learning_rate_b": FloatBoundaryConstraint(min_value=0.01, max_value=0.1)
                                                                })
        hyperparameters_functional_constraints = HyperparametersConstraints({
                                                                    "long_period_above_short_period": IsAboveFunctionalConstraint(id_hyperparameter_above='learning_rate_a', id_hyperparameter_below='learning_rate_b')
                                                                })
        grid_config = {
            'learning_rate_a': 'no_discretisation',
            'learning_rate_b': 'no_discretisation'
        }

        with self.assertRaises(Exception):
            HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
    

    def test_need_minimum_discretisations_1(self):
        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "period": IntBoundaryConstraint(min_value=1, max_value=2)
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'learning_rate_a': 1,
            'period': 'no_discretisation'
        }
        with self.assertRaises(Exception):
            HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)

    def test_need_minimum_discretisations_2(self):
        hyperparameters_boundary_constraints = HyperparametersConstraints({
                                                                    "learning_rate_a": FloatBoundaryConstraint(min_value=0.01, max_value=0.1),
                                                                    "period": IntBoundaryConstraint(min_value=1, max_value=2)
                                                                })
        hyperparameters_functional_constraints = None
        grid_config = {
            'learning_rate_a': 2,
            'period': 1
        }
        with self.assertRaises(Exception) as e:
            HyperparametersGridGenerator().generate_grid(hyperparameters_boundary_constraints, hyperparameters_functional_constraints, grid_config)
