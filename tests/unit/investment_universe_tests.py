import unittest
from cryptobot_commons.investment_universe import InvestmentUniverse


class InvestmentUniverseTestCase(unittest.TestCase):

    def setUp(self):
        # The only thing we need to set is the import of InvestmentUniverse, it is done in the header
        pass

    def test_investment_universe_value_are_int(self):
        is_ok = True
        for element in InvestmentUniverse:
            if type(element.value) == int:
                pass
            else:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_investment_universe_name_are_str(self):
        is_ok = True
        for element in InvestmentUniverse:
            if type(element.name) == str:
                pass
            else:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_investment_universe_name_contain_EUR(self):
        """
        Every name have to contain the 'EUR', indeed we focus only on crypto pair link to EUR for the moment.
        """
        is_ok = True
        for element in InvestmentUniverse:
            if "EUR" in element.name:
                pass
            else:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_investment_universe_name_contain_underscore(self):
        """
        Every cryptopair will have an underscore as separator (eg: "BTC_EUR").
        """
        is_ok = True
        for element in InvestmentUniverse:
            if "_" in element.name:
                pass
            else:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_investment_universe_have_the_BTC_EUR_pair(self):
        """
        The "BTC_EUR" is the most important one, it has to be in InvestmentUniverse.
        """
        is_ok = False
        for element in InvestmentUniverse:
            if element == InvestmentUniverse.BTC_EUR:
                is_ok = True
            else:
                pass
        self.assertEqual(is_ok, True)
