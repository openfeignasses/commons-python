import unittest
from decimal import Decimal

import pandas as pd
from cryptobot_commons.crypto_currency_pair_builder import CryptoCurrencyPairBuilder
from cryptobot_commons.time_series import TimeSeries


class CryptoCurrencyPairBuilderTestCase(unittest.TestCase):

    def test_build_crypto_currency_pair(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1600,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234567
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        crypto_pair = CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
        self.assertTrue(isinstance(crypto_pair.close, TimeSeries))

    def test_build_crypto_convert_to_float1(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1600,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234567
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        crypto_pair = CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
        self.assertTrue(crypto_pair.close.data.dtype == "float64")

    def test_build_crypto_convert_to_float2(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601.00000000,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602.00000000,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        crypto_pair = CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
        self.assertTrue(crypto_pair.close.data.dtype == "float64")

    def test_build_crypto_convert_to_float3(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": Decimal(1601.00000000),
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": Decimal(1602.00000000),
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        crypto_pair = CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
        self.assertTrue(crypto_pair.close.data.dtype == "float64")

    def test_build_crypto_currency_pair_content(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1600,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234567
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        crypto_pair = CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
        self.assertTrue(isinstance(crypto_pair.close, TimeSeries))
        self.assertTrue(isinstance(crypto_pair.close.data, pd.Series))
        self.assertTrue(crypto_pair.close.data.values[0] == 1600)
        self.assertTrue(crypto_pair.close.data.values[1] == 1601)
        self.assertTrue(crypto_pair.close.data.values[2] == 1602)

    def test_column_is_missing1(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1600,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0
        }]

        with self.assertRaises(Exception):
            CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()

    def test_column_is_missing2(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1,
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 2,
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 3,
        }]

        with self.assertRaises(Exception):
            CryptoCurrencyPairBuilder("BTC_EUR", stock_quotations_for_currency_raw).build()
