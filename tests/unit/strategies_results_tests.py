import unittest
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.signal import Signal
from cryptobot_commons.strategy_result import StrategyResult


class StrategiesResultsTestCase(unittest.TestCase):

    def test_instanciate_class(self):
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": None,
            "execution_id": None
        }]
        StrategiesResults(strategies_results_raw)

    def test_wrong_constructor_parameter_1(self):
        with self.assertRaises(Exception):
            StrategiesResults({})

    def test_wrong_constructor_parameter_2(self):
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }, {
        }]
        with self.assertRaises(Exception):
            StrategiesResults(strategies_results_raw)

    def test_wrong_constructor_parameter_3(self):
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
        }]
        with self.assertRaises(Exception):
            StrategiesResults(strategies_results_raw)

    def test_instanciate_with_list_of_StrategyResult(self):
        strategy_result_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.LTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }]
        list_of_strategy_result = [
            StrategyResult(strategy_result_raw[0]),
            StrategyResult(strategy_result_raw[1])
        ]
        StrategiesResults(list_of_strategy_result)
        


