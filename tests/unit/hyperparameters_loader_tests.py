import unittest
from unittest.mock import patch
from cryptobot_commons.hyperparameters.hyperparameters_loader import HyperparametersLoader


class HyperparametersLoaderTestCase(unittest.TestCase):

    def setUp(cls):
        pass

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"short_period": 3, "long_period": 5}'
    })
    def test_instanciate_class(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert isinstance(loaded_hyperparameters.entries, dict)

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS1': '{"short_period": 3, "long_period": 5}',
        'HYPERPARAMETERS_EMA_CROSSOVERS2': '{"short_period": 3, "long_period": 5}',
    })
    def test_equality(self):
        loaded_hyperparameters1 = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS1").load()
        loaded_hyperparameters2 = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS2").load()
        assert loaded_hyperparameters1 == loaded_hyperparameters2

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"short_period": 3, "long_period": 5}'
    })
    def test_get_attribute_1(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert loaded_hyperparameters.entries["short_period"] == 3

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"short_period": 3, "long_period": 5}'
    })
    def test_get_attribute_2(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert loaded_hyperparameters.entries["long_period"] == 5

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"condition": true}'
    })
    def test_load_boolean(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert isinstance(loaded_hyperparameters.entries["condition"], bool)
        assert loaded_hyperparameters.entries["condition"] is True

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"available_models": ["MODEL_1", "MODEL_2"]}'
    })
    def test_load_string_list(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert isinstance(loaded_hyperparameters.entries["available_models"], list)
        assert loaded_hyperparameters.entries["available_models"][0] == "MODEL_1"

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"level": 0.99}'
    })
    def test_load_float(self):
        loaded_hyperparameters = HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
        assert isinstance(loaded_hyperparameters.entries["level"], float)
        assert loaded_hyperparameters.entries["level"] == 0.99

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '["some_value"]'
    })
    def test_load_fails_1(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{}'
    })
    def test_load_fails_2(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"key": null}'
    })
    def test_load_fails_3(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"key": {}}'
    })
    def test_load_fails_4(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"key": []}'
    })
    def test_load_fails_5(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()

    @patch.dict('os.environ', {
        'HYPERPARAMETERS_EMA_CROSSOVERS': '{"key": [1, 2, 3]}'
    })
    def test_load_fails_6(self):
        with self.assertRaises(Exception):
            HyperparametersLoader("HYPERPARAMETERS_EMA_CROSSOVERS").load()
