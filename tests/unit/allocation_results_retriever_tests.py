import unittest
from unittest.mock import MagicMock, patch

from cryptobot_commons.allocation_results import AllocationResults
from cryptobot_commons.repositories import AllocationsResultsRepository
from cryptobot_commons.retrievers.allocation_results_retriever import AllocationResultsRetriever


class AllocationResultsRetrieverTestCase(unittest.TestCase):

    @patch.object(AllocationsResultsRepository, 'get_last_execution', MagicMock(return_value=[{
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 1,
        "value_percent": 1.0,
        "add_date": 1589113875
    }]))
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_last_execution(self, mysql_mock):
        allocation_results_retriever = AllocationResultsRetriever("LAST_EXECUTION")

        retrieved_allocation_results = allocation_results_retriever.execute()
        self.assertIsInstance(retrieved_allocation_results, AllocationResults)
        self.assertTrue(len(retrieved_allocation_results.results) == 1)

    @patch.object(AllocationsResultsRepository, 'get_all', MagicMock(return_value=[{
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 1,
        "value_percent": 1.0,
        "add_date": 1589113875
    }, {
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 2,
        "value_percent": 1.0,
        "add_date": 1589113875
    }]))
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_all(self, mysql_mock):
        allocation_results_retriever = AllocationResultsRetriever("ALL")

        retrieved_allocation_results = allocation_results_retriever.execute()
        self.assertIsInstance(retrieved_allocation_results, AllocationResults)
        self.assertTrue(len(retrieved_allocation_results.results) == 2)

    @patch.object(AllocationsResultsRepository, 'get_limited_ordered', MagicMock(return_value=[{
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 2,
        "value_percent": 1.0,
        "add_date": 1589113875
    }, {
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 1,
        "value_percent": 1.0,
        "add_date": 1589113875
    }]))
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_limited_ordered(self, mysql_mock):
        allocation_results_retriever = AllocationResultsRetriever("LIMITED_ORDERED")

        retrieved_allocation_results = allocation_results_retriever.execute()
        self.assertIsInstance(retrieved_allocation_results, AllocationResults)
        self.assertTrue(len(retrieved_allocation_results.results) == 2)

    @patch.object(AllocationsResultsRepository, 'get_limited_ordered', MagicMock(return_value=[{
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 2,
        "value_percent": 1.0,
        "add_date": 1589113875
    }, {
        "currency": "BTC",
        "allocation_name": "EQUIREPARTITION",
        "execution_id": 1,
        "value_percent": 1.0,
        "add_date": 1589113875
    }]))
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_limited_ordered_custom_limit(self, mysql_mock):
        allocation_results_retriever = AllocationResultsRetriever("LIMITED_ORDERED", limit=10)

        retrieved_allocation_results = allocation_results_retriever.execute()
        self.assertIsInstance(retrieved_allocation_results, AllocationResults)
        self.assertTrue(len(retrieved_allocation_results.results) == 2)

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_fails(self, mysql_mock):
        with self.assertRaises(Exception):
            AllocationResultsRetriever("INEXISTANT_MODE")
