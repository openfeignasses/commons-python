import unittest
import pandas as pd

from cryptobot_commons.classification_metrics import ClassificationMetrics

from cryptobot_commons.signal import Signal
from cryptobot_commons.crypto_currency_pair_builder import CryptoCurrencyPair
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.confusion_matrix_generator import ConfusionMatrixGenerator


def generate_strategies_results(nb_period: int, crypto_pair: CryptoCurrencyPair, signal: Signal):
    strategies_results_list = []
    i = 0
    for _index in crypto_pair.close.data.index:
        if i < len(crypto_pair.close.data) - 5:
            strategies_results_list.append(
                {
                    "strategy_name": "SMA_Crossovers",
                    "currency_pair": InvestmentUniverse.BTC_EUR,
                    "signal": signal,
                    "add_date": _index.to_pydatetime(),
                    "execution_id": 1 + i
                })
        i += 1
    return StrategiesResults(strategies_results_list)


def generate_cryptocurrency_pair(quotes_list: list):
    timestamp_list = [1588496400 + i * 3600 for i in range(len(quotes_list))]
    columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
    df = pd.DataFrame(index=timestamp_list)
    for col in columns_names:
        df[col] = quotes_list
    return CryptoCurrencyPair(name="BTC_EUR", data=df)


class ClassificationMetricsTestCase(unittest.TestCase):

    def setUp(self):
        pass

    # Scenario 1

    def test_scenario_1_1(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.222222, 5)

    def test_scenario_1_2(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.066666, 5)

    def test_scenario_1_3(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.186666, 5)

    # Scenario 1 - consider_neutral=False

    def test_scenario_1_4(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.333333333, 5)

    def test_scenario_1_5(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.1, 5)

    def test_scenario_1_6(self):
        quotes_list = [6, 6, 5, 6, 7, 8, 6, 5, 7, 7, 8, 8, 9, 10, 11, 10, 10, 11, 10, 11, 10, 8, 7]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.0, 5)

    # Scenario 2

    def test_scenario_2_1(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.186666, 5)

    def test_scenario_2_2(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.205128, 5)

    def test_scenario_2_3(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.095238, 5)

    # Scenario 2 - consider_neutral=False

    def test_scenario_2_4(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.28, 5)

    def test_scenario_2_5(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.307692308, 5)

    def test_scenario_2_6(self):
        quotes_list = [9, 10, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9, 6, 3, 4, 9, 8, 7, 6, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.0, 5)
    
    # Scenario 3

    def test_scenario_3_1(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.035087, 5)

    def test_scenario_3_2(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.186666, 5)

    def test_scenario_3_3(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix).compute_f1_score(), 5) == round(0.238095, 5)

    # Scenario 3 - consider_neutral=False

    def test_scenario_3_4(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.LONG)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.052631579, 5)

    def test_scenario_3_5(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.SHORT)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.28, 5)

    def test_scenario_3_6(self):
        quotes_list = [9, 10, 10, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 8]
        foo_crypto_currency_pair = generate_cryptocurrency_pair(quotes_list)
        strategies_results = generate_strategies_results(nb_period=18, crypto_pair=foo_crypto_currency_pair,
                                                         signal=Signal.NEUTRAL)
        confusion_matrix = ConfusionMatrixGenerator(strategies_results, foo_crypto_currency_pair).generate()
        assert round(ClassificationMetrics(confusion_matrix, False).compute_f1_score(), 5) == round(0.0, 5)
