import unittest
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus


class InvestmentUniverseStatusTestCase(unittest.TestCase):

    def test_instanciate_class1(self):
        InvestmentUniverseStatus([{
            "name": InvestmentUniverse.BTC_EUR,
            "status": "TRADABLE",
        }, {
            "name": InvestmentUniverse.GNO_EUR,
            "status": "TRADABLE",
        }])

    def test_instanciate_class2(self):
        investment_universe_status = InvestmentUniverseStatus([])
        assert (isinstance(investment_universe_status.status, dict) and
                not investment_universe_status.status)

    def test_wrong_input_type1(self):
        with self.assertRaises(Exception):
            InvestmentUniverseStatus({})

    def test_wrong_input_type2(self):
        with self.assertRaises(Exception):
            InvestmentUniverseStatus([{
                "name": "BTC_EUR",
                "status": "TRADABLE",
            }, {
                "name": "GNO_EUR",
                "status": "TRADABLE",
            }])

    def test_conversion_to_dict(self):
        investment_universe_status = InvestmentUniverseStatus([{
            "name": InvestmentUniverse.BTC_EUR,
            "status": "TRADABLE",
        }, {
            "name": InvestmentUniverse.GNO_EUR,
            "status": "TRADABLE",
        }])
        assert (investment_universe_status.status[InvestmentUniverse.BTC_EUR] == "TRADABLE" and
                investment_universe_status.status[InvestmentUniverse.GNO_EUR] == "TRADABLE")

    def test_set_new_status1(self):
        investment_universe_status = InvestmentUniverseStatus([{
            "name": InvestmentUniverse.BTC_EUR,
            "status": "TRADABLE",
        }, {
            "name": InvestmentUniverse.GNO_EUR,
            "status": "TRADABLE",
        }])
        investment_universe_status.set_status(InvestmentUniverse.GNO_EUR, "NON_TRADABLE")
        assert investment_universe_status.status[InvestmentUniverse.GNO_EUR] == "NON_TRADABLE"

    def test_set_new_status2(self):
        investment_universe_status = InvestmentUniverseStatus([])
        investment_universe_status.set_status(InvestmentUniverse.GNO_EUR, "NON_TRADABLE")
        assert investment_universe_status.status[InvestmentUniverse.GNO_EUR] == "NON_TRADABLE"

    def test_get_status(self):
        investment_universe_status = InvestmentUniverseStatus([{
            "name": InvestmentUniverse.BTC_EUR,
            "status": "TRADABLE",
        }, {
            "name": InvestmentUniverse.GNO_EUR,
            "status": "TRADABLE",
        }])
        assert investment_universe_status.get_status(InvestmentUniverse.GNO_EUR) == "TRADABLE"
