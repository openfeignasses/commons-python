import unittest
from unittest.mock import patch
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus
from cryptobot_commons.savers.investment_universe_status_saver import InvestmentUniverseStatusSaver


class InvestmentUniverseStatusSaverTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.investment_universe_status_saver = InvestmentUniverseStatusSaver(InvestmentUniverseStatus([{
            "name": InvestmentUniverse.BTC_EUR,
            "status": "TRADABLE",
        }, {
            "name": InvestmentUniverse.GNO_EUR,
            "status": "NON_TRADABLE",
        }]))

    def test_save(self):
        self.investment_universe_status_saver.save()

    def test_save_fails(self):
        self.investment_universe_status_saver.repository = None
        with self.assertRaises(Exception):
            self.investment_universe_status_saver.save()

    def test_wrong_input(self):
        with self.assertRaises(Exception):
            InvestmentUniverseStatusSaver([])
