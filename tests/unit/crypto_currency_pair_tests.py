import unittest
from datetime import datetime
from copy import deepcopy

import pandas as pd

from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.time_series import TimeSeries


class CryptoCurrencyPairTestCase(unittest.TestCase):

    def setUp(self):
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        timestamp_list = [1588496400 + i*3600 for i in range(len(quotes_list))]
        self.columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
        self.df = pd.DataFrame(index=timestamp_list)
        for col in self.columns_names:
            self.df[col] = quotes_list
        self.crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=self.df)

    # Exceptions if inputs are not in the expected format

    def test_crypto_currency_pair_exception_if_input_name_is_not_string(self):
        with self.assertRaises(Exception):
            CryptoCurrencyPair(name=123, data=self.df)

    def test_crypto_currency_pair_exception_if_input_data_is_not_dataframe(self):
        with self.assertRaises(Exception):
            quote_tmp = [1, 1]
            CryptoCurrencyPair(name="BTC_EUR", data=quote_tmp)

    def test_crypto_currency_pair_exception_if_input_data_has_not_expected_columns(self):
        columns_names = ["O", "H", "L", "C", "VW", "V", "C"]
        df_tmp = self.df.copy()
        df_tmp.columns = columns_names
        with self.assertRaises(Exception):
            CryptoCurrencyPair(name="BTC_EUR", data=df_tmp)

    def test_exception_if_input_index_not_datetime_or_int_1(self):
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        timestamp_list = [1588496400 + i*3600 for i in range(len(quotes_list))]
        timestamp_list_float = [float(i) for i in timestamp_list]
        columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
        df = pd.DataFrame(index=timestamp_list_float)
        for col in columns_names:
            df[col] = quotes_list
        with self.assertRaises(Exception):
            crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=df)
    
    def test_exception_if_input_index_not_datetime_or_int_2(self):
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        str_list = ["a_str_variable" for i in range(len(quotes_list))]
        columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
        df = pd.DataFrame(index=str_list)
        for col in columns_names:
            df[col] = quotes_list
        with self.assertRaises(Exception):
            crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=df)


    def test_crypto_currency_pair_no_exception_expected(self):
        CryptoCurrencyPair(name="BTC_EUR", data=self.df)

    # Exceptions if initialized attributes are not in the expected format

    def test_crypto_currency_pair_data_attributes_are_time_series(self):
        self.assertTrue(isinstance(self.crypto_pair.open, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.high, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.low, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.close, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.vwap, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.volume, TimeSeries))
        self.assertTrue(isinstance(self.crypto_pair.count, TimeSeries))

    def test_crypto_currency_pair_initialized_index_is_datetime(self):
        assert isinstance(self.crypto_pair.open.data.index[0], datetime)
        assert isinstance(self.crypto_pair.high.data.index[0], datetime)
        assert isinstance(self.crypto_pair.low.data.index[0], datetime)
        assert isinstance(self.crypto_pair.close.data.index[0], datetime)
        assert isinstance(self.crypto_pair.vwap.data.index[0], datetime)
        assert isinstance(self.crypto_pair.volume.data.index[0], datetime)
        assert isinstance(self.crypto_pair.count.data.index[0], datetime)

    # Others
    
    def test_set_quantity_of_data_1(self):
        new_crypto_pair = deepcopy(self.crypto_pair)
        new_crypto_pair.set_quantity_of_data(number_of_observations=10)
        assert len(new_crypto_pair.open.data) == 10
        assert len(new_crypto_pair.high.data) == 10
        assert len(new_crypto_pair.low.data) == 10
        assert len(new_crypto_pair.close.data) == 10
        assert len(new_crypto_pair.vwap.data) == 10
        assert len(new_crypto_pair.volume.data) == 10
        assert len(new_crypto_pair.count.data) == 10

    def test_set_quantity_of_data_2(self):
        new_crypto_pair = deepcopy(self.crypto_pair)
        new_crypto_pair.set_quantity_of_data(number_of_observations=10, end_of_series=3)
        assert list(new_crypto_pair.close.data.values) == [9.0, 3.0, 6.0, 4.0, 7.0, 8.0, 9.0, 6.0, 5.0, 4.0]

    def test_set_quantity_of_data_exception_if_not_enough_data_in_crypto_pair(self):
        new_crypto_pair = deepcopy(self.crypto_pair)
        with self.assertRaises(Exception):
            new_crypto_pair.set_quantity_of_data(number_of_observations=100)