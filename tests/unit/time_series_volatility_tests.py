import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesVolatilityTestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_volatility_is_panda_series(self):
        self.assertTrue(type(self.time_series.volatility(7))
                        == pd.core.series.Series)

    def test_time_series_volatility_has_good_resultA(self):
        is_ok = True
        volatility = self.time_series.volatility(3)
        volatility_we_expect = [None, None, None, 0.596595278162613, 0.683401080652524, 0.0375770812735241,
                                0.0804166446371264, 0.471865778506102, 0.452055020430698, 0.31655699855371,
                                0.175864999196505, 0.205817513007158, 0.205817513007158, 0.26992149834411,
                                0.323643894381418, 0.440958551844098, 0.875330625366041, 0.694073975255932,
                                0.727323508211241, 0.4064329676609, 0.833680483247718, 0.881917103688197,
                                0.708741712342949, 0.542984181737862, 0.35580105080663, 0.26992149834411,
                                0.231990181784584, 0.0881917103688197, 0.394874569864819, 0.404145188432738,
                                0.352825164504771]
        for i in range(len(volatility_we_expect)):
            if np.isnan(volatility.iloc[i]) and volatility_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(volatility.iloc[i]) and volatility_we_expect[i] is None:
                pass
            elif round(volatility.iloc[i], 5) != round(volatility_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_volatility_has_good_resultB(self):
        is_ok = True
        volatility = self.time_series.volatility(7)
        volatility_we_expect = [None, None, None, None, None, None, None, 0.485737002603352, 0.487914559225265,
                                0.300074564213816, 0.305668950010765, 0.298549151330224, 0.291284785556431,
                                0.301339519586894, 0.258009539634801, 0.304894999695571, 0.581621947738445,
                                0.572370157565638, 0.572340679679088, 0.646243895785102, 0.719220716576935,
                                0.694345231008058, 0.72216498552196, 0.584377006871935, 0.574909383807289,
                                0.605290681127648, 0.524100941920641, 0.384815745346161, 0.394095228368454,
                                0.333880240519229, 0.339800037162641]
        for i in range(len(volatility_we_expect)):
            if np.isnan(volatility.iloc[i]) and volatility_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(volatility.iloc[i]) and volatility_we_expect[i] is None:
                pass
            elif round(volatility.iloc[i], 5) != round(volatility_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_volatility_has_same_index_as_data(self):
        is_ok = True
        volatility = self.time_series.volatility(7)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(volatility.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_volatility_has_nb_NaN_same_as_periodA(self):
        """
        We except to have same number of NaN than period
        """
        volatility = self.time_series.volatility(3)
        count_nan = 0
        for el in volatility:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 3)

    def test_time_series_volatility_has_nb_NaN_same_as_periodB(self):
        """
        We except to have same number of NaN than period
        """
        volatility = self.time_series.volatility(7)
        count_nan = 0
        for el in volatility:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 7)
