import unittest
from datetime import datetime
from unittest.mock import patch
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.savers.strategies_results_saver import StrategiesResultsSaver
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.signal import Signal


class StrategiesResultsSaverTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked",
        'CURRENT_TIMESTAMP': "1234"
    })
    def setUp(cls, mysql_mock):
        cls.strategies_results_saver_1 = StrategiesResultsSaver(StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }]))
        cls.strategies_results_saver_2 = StrategiesResultsSaver(StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": None,
            "execution_id": 1
        }]))

    def test_save_1(self):
        strategies_results = self.strategies_results_saver_1.save()
        assert(isinstance(strategies_results.results[0].add_date, datetime))

    def test_save_1_with_correct_date(self):
        strategies_results = self.strategies_results_saver_1.save()
        assert(strategies_results.results[0].add_date.timestamp() == 1589113875)

    def test_save_2(self):
        strategies_results = self.strategies_results_saver_2.save()
        assert(isinstance(strategies_results.results[0].add_date, datetime))

    def test_save_fails(self):
        self.strategies_results_saver_1.repository = None
        with self.assertRaises(Exception):
            self.strategies_results_saver_1.save()

    def test_wrong_input(self):
        with self.assertRaises(Exception):
            StrategiesResultsSaver([])

    def test_add_date_forced(self):
        strategies_results = self.strategies_results_saver_2.save()
        assert(strategies_results.results[0].add_date.timestamp() == float(1234))