import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesEMATestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5, 6, 8, 10, 11, 14, 17, 17, 16, 13, 10, 8, 9, 6, 3,
                       4, 3, 3, 3, 3, 6, 7, 7, 9, 12, 13, 14, 14, 17, 18, 20]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_ema_is_panda_series(self):
        self.assertTrue(type(self.time_series.ema(7))
                        == pd.core.series.Series)

    def test_time_series_ema_has_good_resultA(self):
        is_ok = True
        ema = self.time_series.ema(7)
        ema_we_expect = [5, 5.25, 5.9375, 6.953125, 7.96484375, 9.4736328125, 11.355224609375, 12.7664184570313,
                         13.5748138427734, 13.4311103820801, 12.5733327865601, 11.42999958992, 10.82249969244,
                         9.61687476933003, 7.96265607699752, 6.97199205774814, 5.9789940433111, 5.23424553248333,
                         4.6756841493625, 4.25676311202187, 4.6925723340164, 5.2694292505123, 5.70207193788423,
                         6.52655395341317, 7.89491546505988, 9.17118659879491, 10.3783899490962, 11.2837924618221,
                         12.7128443463666, 14.034633259775, 15.5259749448312]
        for i in range(len(ema_we_expect)):
            if round(ema.iloc[i], 5) != round(ema_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_ema_has_good_resultB(self):
        is_ok = True
        ema = self.time_series.ema(3)
        ema_we_expect = [5, 5.5, 6.75, 8.375, 9.6875, 11.84375, 14.421875, 15.7109375, 15.85546875, 14.427734375,
                         12.2138671875,
                         10.10693359375, 9.553466796875, 7.7767333984375, 5.38836669921875, 4.69418334960938,
                         3.84709167480469, 3.42354583740234, 3.21177291870117, 3.10588645935059, 4.55294322967529,
                         5.77647161483765, 6.38823580741882, 7.69411790370941, 9.84705895185471, 11.4235294759274,
                         12.7117647379637, 13.3558823689818, 15.1779411844909, 16.5889705922455, 18.2944852961227]

        for i in range(len(ema_we_expect)):
            if round(ema.iloc[i], 5) != round(ema_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_ema_has_same_index_as_data(self):
        is_ok = True
        ema = self.time_series.ema(3)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(ema.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)
