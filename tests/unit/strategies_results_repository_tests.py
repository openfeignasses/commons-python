import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import StrategiesResultsRepository


class StrategiesResultsRepositoryTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.strategies_results_repository = StrategiesResultsRepository()
        cls.strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }])

    def test_get_all(self):
        result = self.strategies_results_repository.get_all()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)

    def test_get_last_execution(self):
        result = self.strategies_results_repository.get_last_execution()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)
