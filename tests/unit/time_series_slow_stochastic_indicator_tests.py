import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries

def check_indicators(slow_k, result_expected_k, slow_d, result_expected_d, period_k, mean_period_k, mean_period_d):
    counter = 0
    for i in range(period_k+mean_period_k-2, len(slow_k)):
        assert round(slow_k.iloc[i], 5) == round(result_expected_k[counter], 5)
        counter += 1
    counter = 0
    for i in range(period_k+mean_period_k+mean_period_d-3, len(slow_d)):
        assert round(slow_d.iloc[i], 5) == round(result_expected_d[counter], 5)
        counter += 1

class SlowStochasticIndicatorTestCase(unittest.TestCase):

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list_1 = [6,6,5,6,7,8,6,5,7,7,8,8,9,10,11,10,10,11,10,11,10,8,7]
        quotes_list_2 = [9,10,12,10,8,7,5,8,8,9,7,8,9,6,3,4,9,8,7,6,8,9,8]
        quotes_list_3 = [9,10,10,9,11,12,14,16,20,21,20,21,19,18,15,14,12,11,10,8,8,9,7]
        # Date generation
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list_1)) + datetime.timedelta(days=i) for i in range(len(quotes_list_1))]
        # We can then initialize the TimeSeries object
        series_1 = pd.Series(quotes_list_1, index=dates_list, name='BTC_EUR')
        self.time_series_1 = TimeSeries("BTC_EUR", series_1)
        series_2 = pd.Series(quotes_list_2, index=dates_list, name='BTC_EUR')
        self.time_series_2 = TimeSeries("BTC_EUR", series_2)
        series_3 = pd.Series(quotes_list_3, index=dates_list, name='BTC_EUR')
        self.time_series_3 = TimeSeries("BTC_EUR", series_3)

    def test_time_series_slow_stochastic_indicator_is_panda_series(self):
        slow_k_1, slow_d_1 = self.time_series_1.slow_stochastic_indicator(period_k=7, mean_period_k=3, mean_period_d=3)
        slow_k_2, slow_d_2 = self.time_series_2.slow_stochastic_indicator(period_k=6, mean_period_k=3, mean_period_d=3)
        slow_k_3, slow_d_3 = self.time_series_2.slow_stochastic_indicator(period_k=5, mean_period_k=3, mean_period_d=3)
        assert  isinstance(slow_k_1, pd.Series)
        assert  isinstance(slow_d_1, pd.Series)
        assert  isinstance(slow_k_2, pd.Series)
        assert  isinstance(slow_d_2, pd.Series)
        assert  isinstance(slow_k_3, pd.Series)
        assert  isinstance(slow_d_3, pd.Series)        

    def test_time_series_slow_stochastic_indicator_has_good_result_a(self):
        slow_k, slow_d = self.time_series_1.slow_stochastic_indicator(period_k=7, mean_period_k=3, mean_period_d=3)
        result_expected_k = [33.3333333333333,44.4444444444444,77.7777777777778,88.8888888888889,100,100,100,91.6666666666667,80.5555555555556,80.5555555555556,72.2222222222222,83.3333333333333,50,33.3333333333333,0]
        result_expected_d = [51.8518518518519,70.3703703703704,88.8888888888889,96.2962962962963,100,97.2222222222222,90.7407407407407,84.2592592592593,77.7777777777778,78.7037037037037,68.5185185185185,55.5555555555556,27.7777777777778]
        check_indicators(slow_k, result_expected_k, slow_d, result_expected_d, 7, 3, 3)

    def test_time_series_slow_stochastic_indicator_has_good_result_b(self):
        slow_k, slow_d = self.time_series_2.slow_stochastic_indicator(period_k=6, mean_period_k=3, mean_period_d=3)
        result_expected_k = [14.2857142857143,34.2857142857143,67.6190476190476,70,75,75,58.3333333333333,33.3333333333333,5.55555555555556,38.8888888888889,66.6666666666667,83.3333333333333,66.6666666666667,65.5555555555556,76.6666666666667,82.2222222222222]
        result_expected_d = [38.7301587301587,57.3015873015873,70.8730158730159,73.3333333333333,69.4444444444444,55.5555555555556,32.4074074074074,25.9259259259259,37.037037037037,62.962962962963,72.2222222222222,71.8518518518518,69.6296296296296,74.8148148148148]
        check_indicators(slow_k, result_expected_k, slow_d, result_expected_d, 6, 3, 3)

    def test_time_series_slow_stochastic_indicator_has_good_result_c(self):
        slow_k, slow_d = self.time_series_3.slow_stochastic_indicator(period_k=5, mean_period_k=3, mean_period_d=3)
        result_expected_k = [100,100,100,100,95.2380952380952,95.2380952380952,61.9047619047619,33.3333333333333,0,0,0,0,0,0,0,11.1111111111111,11.1111111111111]
        result_expected_d = [100,100,98.4126984126984,96.8253968253968,84.1269841269841,63.4920634920635,31.7460317460317,11.1111111111111,0,0,0,0,0,3.7037037037037,7.40740740740741]
        check_indicators(slow_k, result_expected_k, slow_d, result_expected_d, 5, 3, 3)

    def test_time_series_stochastic_indicator_has_good_nb_NaN_a(self):
        slow_k, slow_d = self.time_series_1.slow_stochastic_indicator(period_k=7, mean_period_k=3, mean_period_d=3)
        count_nan = 0
        for el in slow_k:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 8
        count_nan = 0
        for el in slow_d:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 10

    def test_time_series_stochastic_indicator_has_good_nb_NaN_b(self):
        slow_k, slow_d = self.time_series_2.slow_stochastic_indicator(period_k=6, mean_period_k=3, mean_period_d=3)
        count_nan = 0
        for el in slow_k:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 7
        count_nan = 0
        for el in slow_d:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 9

    def test_time_series_stochastic_indicator_has_good_nb_NaN_c(self):
        slow_k, slow_d = self.time_series_3.slow_stochastic_indicator(period_k=5, mean_period_k=3, mean_period_d=3)
        count_nan = 0
        for el in slow_k:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 6
        count_nan = 0
        for el in slow_d:
            if np.isnan(el):
                count_nan += 1
        assert count_nan == 8