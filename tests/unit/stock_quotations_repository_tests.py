import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import StockQuotationsRepository


class StockQuotationRepositoryTestCase(unittest.TestCase):

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_get_by_currency(self, mysql_mock):
        stock_quotations_repository = StockQuotationsRepository()
        stock_quotations_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "close": 1600,
            "time": 1234567,
            "currency_pair": "BTC_EUR",
            "add_date": 1589113875
        }, {
            "close": 1601,
            "time": 1234568,
            "currency_pair": "BTC_EUR",
            "add_date": 1589113875
        }, {
            "close": 1602,
            "time": 1234569,
            "currency_pair": "BTC_EUR",
            "add_date": 1589113875
        }])

        result = stock_quotations_repository.get_by_currency("BTC_EUR")
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)
        self.assertTrue(type(result[0]["close"]) == int)
        self.assertTrue(type(result[0]["currency_pair"]) == str)
        self.assertTrue(type(result[0]["add_date"]) == int)

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_get_distinct_currency_pairs(self, mysql_mock):
        stock_quotations_repository = StockQuotationsRepository()
        stock_quotations_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "currency_pair": "BTC_EUR",
        }, {
            "currency_pair": "ETH_EUR",
        }])

        result = stock_quotations_repository.get_distinct_currency_pairs()
        self.assertTrue(type(result) == list)
        self.assertTrue(result[0]["currency_pair"] == "BTC_EUR")
        self.assertTrue(result[1]["currency_pair"] == "ETH_EUR")
