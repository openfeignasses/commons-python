import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesRSIUpDownTestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5.9, 6.1, 8.4, 10.7, 11.3, 14.5, 17.1, 17.6, 18.2, 19.4, 21.8, 22.1, 23.5, 27.7, 29.4, 30.2,
                       28.3, 26.7, 25.4, 23.1, 22.2, 20.9, 20.5, 18.6, 15.3, 15.3, 14.1, 14.1, 13.0, 11.8, 9.9, 8.3]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_rsi_is_panda_series(self):
        self.assertTrue(type(self.time_series.rsi(14))
                        == pd.core.series.Series)

    def test_time_series_rsi_has_good_result(self):
        is_ok = True
        rsi = self.time_series.rsi(14)
        rsi_we_expect = [None, None, None, None, None, None, None, None, None, None, None, None, None, None,
                         100, 100, 91.9831223628692, 84.7826086956522, 79.746835443038, 68.859649122807,
                         62.085308056872, 57.5342465753425, 55.2995391705069, 48.2142857142857, 36.0515021459227,
                         35.2173913043478, 29.3859649122807, 13.4408602150538, 4.44444444444446, 0, 0, 0]
        for i in range(len(rsi_we_expect)):
            if np.isnan(rsi.iloc[i]) and rsi_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(rsi.iloc[i]) and rsi_we_expect[i] is None:
                pass
            elif round(rsi.iloc[i], 5) != round(rsi_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_rsi_has_same_index_as_data(self):
        is_ok = True
        rsi = self.time_series.rsi(14)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(rsi.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_rsi_has_nb_NaN_same_as_periodA(self):
        """
        We except to have same number of NaN than period - 1
        """
        rsi = self.time_series.rsi(14)
        count_nan = 0
        for el in rsi:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 14)