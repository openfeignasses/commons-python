import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import AllocationsResultsRepository


class AllocationsResultsRepositoryTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.allocations_results_repository = AllocationsResultsRepository(None, None, None, None)
        cls.allocations_results_repository.db.cursor().fetchall = MagicMock(return_value=[])

    def test_get_all(self):
        result = self.allocations_results_repository.get_all()
        self.assertTrue(type(result) == list)
        self.assertTrue(len(result) == 0)

    def test_get_last_execution(self):
        result = self.allocations_results_repository.get_last_execution()
        self.assertTrue(type(result) == list)
        self.assertTrue(len(result) == 0)

    def test_get_max_execution_id(self):
        self.allocations_results_repository.db.cursor().fetchone = MagicMock(return_value={
            'max_execution_id': None
        })
        self.assertTrue(self.allocations_results_repository.get_max_execution_id() is None)