import unittest
import pandas as pd
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs


class CryptoCurrencyPairsTestCase(unittest.TestCase):

    def setUp(self):
        stock_quotations_for_currency_raw = [{
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1600,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234567
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1601,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234568
        }, {
            "open": 0.0,
            "high": 0.0,
            "low": 0.0,
            "close": 1602,
            "vwap": 0.0,
            "volume": 0.0,
            "count": 0.0,
            "time": 1234569
        }]
        dataframe = pd.DataFrame(data=stock_quotations_for_currency_raw).set_index("time")
        self.crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=dataframe)

    def test_instanciate(self):
        CryptoCurrencyPairs([self.crypto_pair, self.crypto_pair])

    def test_bad_input(self):
        with self.assertRaises(Exception):
            CryptoCurrencyPairs(None)
