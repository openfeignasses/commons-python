import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesSMATestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_sma_is_panda_series(self):
        self.assertTrue(type(self.time_series.sma(7))
                        == pd.core.series.Series)

    def test_time_series_sma_has_good_resultA(self):
        is_ok = True
        sma = self.time_series.sma(7)
        sma_we_expect = [None, None, None, None, None, None, 7.57142857142857, 8, 8.28571428571429, 7.85714285714286,
                         7.42857142857143, 7.42857142857143, 7.71428571428571, 7.85714285714286, 7.14285714285714,
                         6.57142857142857, 6.57142857142857,
                         6.71428571428571, 6.85714285714286, 6, 6, 6.14285714285714, 6.57142857142857, 6.42857142857143,
                         6.57142857142857, 6.14285714285714, 6.42857142857143, 6.14285714285714, 6.42857142857143,
                         6.71428571428571, 6.71428571428571]
        for i in range(len(sma_we_expect)):
            if np.isnan(sma.iloc[i]) and sma_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(sma.iloc[i]) and sma_we_expect[i] is None:
                pass
            elif round(sma.iloc[i], 5) != round(sma_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_sma_has_good_resultB(self):
        is_ok = True
        sma = self.time_series.sma(3)
        sma_we_expect = [None, None, 7.66666666666667, 9.33333333333333, 10, 8.33333333333333, 6.66666666666667,
                         6.66666666666667, 7, 8.33333333333333, 8, 8, 8, 7.66666666666667, 6, 4.33333333333333,
                         5.33333333333333, 7, 8.66666666666667, 6.66666666666667, 6, 4.33333333333333, 5.66666666666667,
                         6.33333333333333, 8, 7.66666666666667, 6.66666666666667, 5, 5, 6.33333333333333,
                         7.66666666666667]
        for i in range(len(sma_we_expect)):
            if np.isnan(sma.iloc[i]) and sma_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(sma.iloc[i]) and sma_we_expect[i] is None:
                pass
            elif round(sma.iloc[i], 5) != round(sma_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_sma_has_same_index_as_data(self):
        is_ok = True
        sma = self.time_series.sma(3)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(sma.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_sma_has_nb_NaN_same_as_periodA(self):
        """
        We except to have same number of NaN than period - 1
        """
        sma = self.time_series.sma(7)
        count_nan = 0
        for el in sma:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 6)

    def test_time_series_sma_has_nb_NaN_same_as_periodB(self):
        """
        We except to have same number of NaN than period - 1
        """
        sma = self.time_series.sma(3)
        count_nan = 0
        for el in sma:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 2)
