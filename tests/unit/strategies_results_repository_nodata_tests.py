import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import StrategiesResultsRepository


class StrategiesResultsRepositoryTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.strategies_results_repository = StrategiesResultsRepository()
        cls.strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=[])

    def test_get_all_nodata(self):
        result = self.strategies_results_repository.get_all()
        self.assertTrue(type(result) == list)
        self.assertTrue(len(result) == 0)

    def test_get_last_execution_nodata(self):
        result = self.strategies_results_repository.get_last_execution()
        self.assertTrue(type(result) == list)
        self.assertTrue(len(result) == 0)
