import unittest
import numpy as np
import pandas as pd

from cryptobot_commons.utils import type_checking


@type_checking
def test_function_a(a: int, b: str, c: bool):
    pass

@type_checking
def test_function_b(a: int, b: np.ndarray):
    pass

@type_checking
def test_function_c(a: str, b: pd.DataFrame):
    pass


class TypeCheckingTestCase(unittest.TestCase):

    def test_function_a_is_ok(self):
        test_function_a(1, 'is_ok', True)

    def test_function_a_is_not_ok_1(self):
        with self.assertRaises(Exception):
            test_function_a(1, 'is_ok', 'test_wrong_arg')

    def test_function_a_is_not_ok_2(self):
        with self.assertRaises(Exception):
            test_function_a(1.0, 'is_ok', True)

    def test_function_b_is_ok(self):
        test_function_b(1, np.array([]))

    def test_function_b_is_not_ok_1(self):
        with self.assertRaises(Exception):
            test_function_b(1, 'test_wrong_arg')

    def test_function_b_is_not_ok_2(self):
        with self.assertRaises(Exception):
            test_function_b(1.0, np.array([]))

    def test_function_c_is_ok(self):
        test_function_c('a string', pd.DataFrame())

    def test_function_c_is_not_ok_1(self):
        with self.assertRaises(Exception):
            test_function_c('a string', 'test_wrong_arg')

    def test_function_c_is_not_ok_2(self):
        with self.assertRaises(Exception):
            test_function_c(1, pd.DataFrame())