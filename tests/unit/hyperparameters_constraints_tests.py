import unittest
from cryptobot_commons.hyperparameters.boolean_boundary_constraint import BooleanBoundaryConstraint
from cryptobot_commons.hyperparameters.float_boundary_constraint import FloatBoundaryConstraint
from cryptobot_commons.hyperparameters.int_boundary_constraint import IntBoundaryConstraint
from cryptobot_commons.hyperparameters.string_boundary_constraint import StringBoundaryConstraint
from cryptobot_commons.hyperparameters.is_above_functional_constraint import IsAboveFunctionalConstraint
from cryptobot_commons.hyperparameters.hyperparameters_constraints import HyperparametersConstraints
from cryptobot_commons.hyperparameters.hyperparameters_loader import HyperparametersLoader


class HyperparametersConstraintsTestCase(unittest.TestCase):

    def setUp(cls):
        pass

    #-------------------------------------------------
    # Class instanciation

    def test_instanciate_class_int_constraints(self):
        HyperparametersConstraints({
            "some_short_period": IntBoundaryConstraint(1, 99),
            "some_long_period": IntBoundaryConstraint(1, 99)
        })

    def test_instanciate_class_float_constraints(self):
        HyperparametersConstraints({
            "some_level_1": FloatBoundaryConstraint(0.0, 1.0),
            "some_level_2": FloatBoundaryConstraint(0.0, 1.0)
        })

    def test_instanciate_class_mixed_constraints(self):
        HyperparametersConstraints({
            "some_level_1": FloatBoundaryConstraint(0.0, 1.0),
            "some_period": IntBoundaryConstraint(1, 99)
        })

    def test_instanciate_class_boolean_constraint(self):
        HyperparametersConstraints({
            "some_flag": BooleanBoundaryConstraint()
        })

    def test_instanciate_class_string_constraint(self):
        HyperparametersConstraints({
            "some_algorithm_to_use": StringBoundaryConstraint(["ALGO_1", "ALGO_2"])
        })

    def test_instanciate_class_is_above_constraint(self):
        HyperparametersConstraints({
            "a_functional_constraint": IsAboveFunctionalConstraint("HYPERPARAMETER_1", "HYPERPARAMETER_2")
        })


    #-------------------------------------------------
    # Unhappy paths

    def test_instanciate_class_fails(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({})

    def test_instanciate_class_fails_2(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_short_period": IntBoundaryConstraint("1", "99"),
            })

    def test_instanciate_class_fails_3(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_level": FloatBoundaryConstraint("1.0", "2.0"),
            })

    def test_instanciate_class_fails_4(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_algorithm_to_use": StringBoundaryConstraint(),
            })

    def test_instanciate_class_fails_5(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_algorithm_to_use": StringBoundaryConstraint("a"),
            })

    def test_instanciate_class_fails_5(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_algorithm_to_use": StringBoundaryConstraint([]),
            })

    def test_instanciate_class_fails_6(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_algorithm_to_use": StringBoundaryConstraint([1]),
            })

    def test_instanciate_class_fails_7(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_algorithm_to_use": StringBoundaryConstraint(["ALGO_1", 1]),
            })


    def test_instanciate_class_fails_8(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_short_period": IntBoundaryConstraint(99, 1),
            })

    
    def test_instanciate_class_fails_9(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_short_period": IntBoundaryConstraint(1.5, 99.5),
            })

    def test_instanciate_class_fails_10(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "some_short_period": FloatBoundaryConstraint(99.5, 1.5),
            })

    def test_instanciate_class_fails_11(self):
        with self.assertRaises(Exception):
            HyperparametersConstraints({
                "a_functional_constraint": IsAboveFunctionalConstraint(1, 99)
            })