import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import StrategiesResultsRepository
from cryptobot_commons.retrievers.strategies_results_retriever import StrategiesResultsRetriever
from cryptobot_commons.strategies_results import StrategiesResults


class StrategiesResultsRetrieverTestCase(unittest.TestCase):

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_last_execution(self, mysql_mock):
        strategies_results_repository = StrategiesResultsRepository()
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }]
        strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=strategies_results_raw)
        strategies_results_retriever = StrategiesResultsRetriever("LAST_EXECUTION")

        retrieved_strategies_results = strategies_results_retriever.execute()
        self.assertIsInstance(retrieved_strategies_results, StrategiesResults)
        self.assertTrue(len(retrieved_strategies_results.results) == len(strategies_results_raw))

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_all(self, mysql_mock):
        strategies_results_repository = StrategiesResultsRepository()
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "SHORT",
            "add_date": 1589113875,
            "execution_id": 2
        }]
        strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=strategies_results_raw)
        strategies_results_retriever = StrategiesResultsRetriever("ALL")

        retrieved_strategies_results = strategies_results_retriever.execute()
        self.assertIsInstance(retrieved_strategies_results, StrategiesResults)
        self.assertTrue(len(retrieved_strategies_results.results) == len(strategies_results_raw))

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_limited_ordered(self, mysql_mock):
        strategies_results_repository = StrategiesResultsRepository()
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "SHORT",
            "add_date": 1589113875,
            "execution_id": 2
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }]
        strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=strategies_results_raw)
        strategies_results_retriever = StrategiesResultsRetriever("LIMITED_ORDERED")

        retrieved_strategies_results = strategies_results_retriever.execute()
        self.assertIsInstance(retrieved_strategies_results, StrategiesResults)
        self.assertTrue(len(retrieved_strategies_results.results) == len(strategies_results_raw))

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_limited_ordered_custom_limit(self, mysql_mock):
        strategies_results_repository = StrategiesResultsRepository()
        strategies_results_raw = [{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "SHORT",
            "add_date": 1589113875,
            "execution_id": 2
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }]
        strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=strategies_results_raw)
        strategies_results_retriever = StrategiesResultsRetriever("LIMITED_ORDERED", limit=10)

        retrieved_strategies_results = strategies_results_retriever.execute()
        self.assertIsInstance(retrieved_strategies_results, StrategiesResults)
        self.assertTrue(len(retrieved_strategies_results.results) == len(strategies_results_raw))

    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_by_strategy(self, mysql_mock):
        strategies_results_repository = StrategiesResultsRepository()
        strategies_results_raw = [{
            "strategy_name": "EMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": 1589113875,
            "execution_id": 1
        }]
        strategies_results_repository.db.cursor().fetchall = MagicMock(return_value=strategies_results_raw)
        strategies_results_retriever = StrategiesResultsRetriever("BY_STRATEGY", strategy_name="EMA_CROSSOVERS")

        retrieved_strategies_results = strategies_results_retriever.execute()
        self.assertIsInstance(retrieved_strategies_results, StrategiesResults)
        self.assertTrue(len(retrieved_strategies_results.results) == 1)


    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_fails(self, mysql_mock):
        with self.assertRaises(Exception):
            StrategiesResultsRetriever("INEXISTANT_MODE")
