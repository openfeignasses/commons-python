import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesRSIDownUpTestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [30.7, 29.6, 28.2, 26.5, 26.1, 25.3, 24.2, 22.1, 18.2, 17.2, 17.2, 15.9, 14.4, 13.1, 10.6,
                       12.3, 13.7, 14.8, 16.3, 18.1, 20.4, 23.4, 23.4, 25.0, 25.1, 27.8, 29.6, 30.3, 31.2, 33.5,
                       34.6, 36.0]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_rsi_is_panda_series(self):
        self.assertTrue(type(self.time_series.rsi(14))
                        == pd.core.series.Series)

    def test_time_series_rsi_has_good_result(self):
        is_ok = True
        rsi = self.time_series.rsi(14)
        rsi_we_expect = [None, None, None, None, None, None, None, None, None, None, None, None, None, None,
                         0.0, 8.21256038647344, 14.975845410628, 20.8955223880597, 26.8867924528302, 33.7837837837838,
                         41.8803418803419, 52.6748971193416, 62.7450980392157, 68.5714285714286, 68.7203791469194,
                         76.4444444444444, 83.3333333333333, 88.7387387387387, 100.0, 100.0, 100.0, 100.0]
        for i in range(len(rsi_we_expect)):
            if np.isnan(rsi.iloc[i]) and rsi_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(rsi.iloc[i]) and rsi_we_expect[i] is None:
                pass
            elif round(rsi.iloc[i], 5) != round(rsi_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_rsi_has_same_index_as_data(self):
        is_ok = True
        rsi = self.time_series.rsi(14)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(rsi.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_rsi_has_nb_NaN_same_as_periodA(self):
        """
        We except to have same number of NaN than period - 1
        """
        rsi = self.time_series.rsi(14)
        count_nan = 0
        for el in rsi:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 14)