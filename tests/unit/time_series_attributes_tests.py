import unittest
import datetime
import pandas as pd
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesAttributesTestCase(unittest.TestCase):

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        self.quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                            6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        self.dates_list = [base - datetime.timedelta(days=len(
            self.quotes_list)) + datetime.timedelta(days=i) for i in range(len(self.quotes_list))]
        self.serie = pd.Series(self.quotes_list, index=self.dates_list, name='BTC_EUR')

    def test_time_series_exeption_if_name_is_not_str(self):
        with self.assertRaises(Exception):
            TimeSeries(123, self.serie)

    def test_time_series_exeption_if_name_dont_contains_EUR_and_underscore(self):
        with self.assertRaises(Exception):
            TimeSeries("BTC", self.serie)

    def test_time_series_no_exeption_if_name_contains_EUR_and_underscore(self):
        TimeSeries("123_EUR", self.serie)

    def test_time_series_exception_if_data_is_not_panda_series(self):
        serie = ["trolololol"]
        with self.assertRaises(Exception):
            TimeSeries("BTC", serie)

    def test_time_series_no_exeption_if_input_data_are_str_convertible(self):
        series_temp = pd.Series(["11", "22"], index=[1, 2], name='BTC_EUR')
        TimeSeries("123_EUR", series_temp)

    def test_time_series_exeption_if_input_data_are_str_not_convertible(self):
        series_temp = pd.Series(["this", "has", "to", "crash"], index=[1, 2, 3, 4], name='BTC_EUR')
        with self.assertRaises(Exception):
            TimeSeries("123_EUR", series_temp)

    def test_time_series_data_attribute_is_series_of_float(self):
        series_temp = pd.Series(["11", "22"], index=[1, 2], name='BTC_EUR')
        ts = TimeSeries("123_EUR", series_temp)
        self.assertTrue(ts.data.dtype == "float64")

    def test_time_series_no_exception_if_data_is_panda_series(self):
        TimeSeries("123_EUR", self.serie)

    def test_time_series_exception_if_start_date_is_not_dateTime(self):
        quotes_list = [1, 1]
        dates_list = ["29-12-2019", datetime.datetime.strptime("30-12-2019", "%d-%m-%Y")]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        with self.assertRaises(Exception):
            TimeSeries("123_EUR", serie)

    def test_time_series_no_exception_if_start_date_is_dateTime(self):
        TimeSeries("123_EUR", self.serie)

    def test_time_series_exception_if_end_date_is_not_dateTime(self):
        quotes_list = [1, 1]
        dates_list = [datetime.datetime.strptime("29-12-2019", "%d-%m-%Y"), "30-12-2019"]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        with self.assertRaises(Exception):
            TimeSeries("123_EUR", serie)

    def test_time_series_no_exception_if_end_date_is_dateTime(self):
        TimeSeries("123_EUR", self.serie)

    def test_time_series_exception_if_end_date_is_before_start_date(self):
        quotes_list = [1, 1]
        dates_list = [datetime.datetime.strptime("30-12-2019", "%d-%m-%Y"),
                      datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        with self.assertRaises(Exception):
            TimeSeries("123_EUR", serie)

    def test_time_series_no_exception_if_end_date_is_after_start_date(self):
        TimeSeries("123_EUR", self.serie)
