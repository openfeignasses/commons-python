import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.repositories import AllocationsResultsRepository


class AllocationsResultsRepositoryTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.allocations_results_repository = AllocationsResultsRepository(None, None, None, None)
        cls.allocations_results_repository.db.cursor().fetchall = MagicMock(return_value=[{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 1,
            "value_percent": 100,
            "add_date": 1589113875,
        }, {
            "currency": "EUR",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 1,
            "value_percent": 0,
            "add_date": 1589113875,
        }])

    def test_get_all(self):
        result = self.allocations_results_repository.get_all()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)

    def test_get_last_execution(self):
        result = self.allocations_results_repository.get_last_execution()
        self.assertTrue(type(result) == list)
        self.assertTrue(type(result[0]) == dict)
        self.assertTrue(len(result) >= 2)

    def test_get_max_execution_id(self):
        self.allocations_results_repository.db.cursor().fetchone = MagicMock(return_value={
            'max_execution_id': 10
        })
        self.assertTrue(self.allocations_results_repository.get_max_execution_id() == 10)
