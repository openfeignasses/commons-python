import unittest
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.signal import Signal
from cryptobot_commons.strategy_result import StrategyResult


class StrategyResultTestCase(unittest.TestCase):

    def test_instanciate_class1(self):
        strategy_result_raw = {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }
        StrategyResult(strategy_result_raw)

    def test_instanciate_class2(self):
        strategy_result_raw = {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": None,
            "execution_id": 1
        }
        StrategyResult(strategy_result_raw)

    def test_instanciate_class3(self):
        strategy_result_raw = {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": None,
            "execution_id": None
        }
        StrategyResult(strategy_result_raw)

    def test_wrong_constructor_parameter(self):
        with self.assertRaises(Exception):
            StrategyResult({})

    def test_wrong_add_date_format(self):
        strategy_result_raw = {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": "LONG",
            "add_date": "10/10/2020 12:00:00",
            "execution_id": 1
        }
        with self.assertRaises(Exception):
            StrategyResult(strategy_result_raw)

    def test_wrong_currency_pair_format(self):
        strategy_result_raw = {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": "BTC_EUR",
            "signal": "LONG",
            "add_date": "10/10/2020 12:00:00",
            "execution_id": 1
        }
        with self.assertRaises(Exception):
            StrategyResult(strategy_result_raw)
