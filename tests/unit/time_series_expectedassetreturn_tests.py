import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesExpectedAssetReturnTestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods".
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_expected_asset_return_is_panda_series(self):
        self.assertTrue(
            type(self.time_series.expected_asset_return(1, 5)) == pd.core.series.Series)

    def test_time_series_expected_asset_return_has_good_resultA(self):
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(3, 5)
        expected_returns_we_expect = [None, None, None, None, None, None, None, 0.0833333333333333, -0.0880952380952381,
                                      0.00523809523809524, 0.0635714285714286, 0.163571428571429, 0.163571428571429,
                                      0.106428571428571, -0.178571428571429, -0.26468253968254, -0.16468253968254,
                                      0.168650793650794, 0.447222222222222, 0.438888888888889, 0.5, 0.288888888888889,
                                      0.222222222222222, 0.0388888888888889, 0.422222222222222, 0.443650793650794,
                                      0.479761904761905, 0.101984126984127, 0.0353174603174603, -0.0546825396825397,
                                      0.173888888888889]
        for i in range(len(expected_returns_we_expect)):
            if np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] != None:
                is_ok = False
            elif np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] == None:
                pass
            elif round(expected_returns.iloc[i], 5) != round(expected_returns_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_expected_asset_return_has_good_resultB(self):
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(1, 3)
        expected_returns_we_expect = [None, None, None, 0.344444444444444, 0.211111111111111, -0.163888888888889,
                                      -0.203571428571429, 0.0630952380952381, 0.104761904761905, 0.241666666666667,
                                      -0.0324074074074074, 0.0152116402116402, 0.0152116402116402, -0.0218253968253968,
                                      -0.236111111111111, -0.166666666666667, 0.361111111111111, 0.490740740740741,
                                      0.421296296296296, -0.217592592592593, 0.152777777777778, 0, 0.472222222222222,
                                      0.186507936507937, 0.339285714285714, -0.0218253968253968, -0.125,
                                      -0.233333333333333, 0.0444444444444444, 0.266666666666667, 0.296296296296296]
        for i in range(len(expected_returns_we_expect)):
            if np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] is None:
                pass
            elif round(expected_returns.iloc[i], 5) != round(expected_returns_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_expected_asset_return_has_good_resultC(self):
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(3, 2)
        expected_returns_we_expect = [None, None, None, None, 0.666666666666667, -0.0416666666666667,
                                      -0.458333333333333, -0.25, 0.0714285714285714, 0.471428571428571, 0.3375, -0.0625,
                                      0, -0.0714285714285714, -0.383928571428571, -0.590277777777778,
                                      -0.0277777777777778, 1.08333333333333, 1.45833333333333, 0.291666666666667,
                                      -0.458333333333333, -0.402777777777778, 0.388888888888889, 0.833333333333333,
                                      0.791666666666667, 0.553571428571429, -0.258928571428571, -0.465277777777778,
                                      -0.277777777777778, 0.4, 0.9]
        for i in range(len(expected_returns_we_expect)):
            if np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(expected_returns.iloc[i]) and expected_returns_we_expect[i] is None:
                pass
            elif round(expected_returns.iloc[i], 5) != round(expected_returns_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_expected_asset_return_has_same_index_as_data(self):
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(3, 5)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(expected_returns.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_expected_asset_return_has_nb_NaN_same_as_periodA(self):
        """
        We except to have same number of NaN than periodChange + periodExpectation - 1
        """
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(3, 5)
        count_nan = 0
        for el in expected_returns:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 7)

    def test_time_series_expected_asset_return_has_nb_NaN_same_as_periodB(self):
        """
        We except to have same number of NaN than periodChange + periodExpectation - 1
        """
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(1, 3)
        count_nan = 0
        for el in expected_returns:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 3)

    def test_time_series_expected_asset_return_has_nb_NaN_same_as_periodC(self):
        """
        We except to have same number of NaN than periodChange + periodExpectation - 1
        """
        is_ok = True
        expected_returns = self.time_series.expected_asset_return(3, 2)
        count_nan = 0
        for el in expected_returns:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 4)
