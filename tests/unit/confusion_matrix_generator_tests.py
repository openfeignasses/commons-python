import unittest
from cryptobot_commons.signal import Signal
import pandas as pd
from cryptobot_commons.confusion_matrix_generator import ConfusionMatrixGenerator
from cryptobot_commons.confusion_matrix import ConfusionMatrix
from cryptobot_commons.investment_universe import InvestmentUniverse
from cryptobot_commons.strategies_results import StrategiesResults
from cryptobot_commons.crypto_currency_pair import CryptoCurrencyPair
from cryptobot_commons.crypto_currency_pairs import CryptoCurrencyPairs
from cryptobot_commons.strategies_results_sorter import StrategiesResultsSorter
from cryptobot_commons.book_of_confusion_matrixes import BookOfConfusionMatrixes


class ConfusionMatrixGeneratorTestCase(unittest.TestCase):

    def setUp(self):
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        timestamp_list = [1588496400 + i*3600 for i in range(len(quotes_list))]
        self.columns_names = ["open", "high", "low", "close", "vwap", "volume", "count"]
        self.df = pd.DataFrame(index=timestamp_list)
        for col in self.columns_names:
            self.df[col] = quotes_list
        self.crypto_pair = CryptoCurrencyPair(name="BTC_EUR", data=self.df)
        self.crypto_pair_bis = CryptoCurrencyPair(name="ETH_EUR", data=self.df)

    def test_instanciate_class(self):

        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])
        ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)

    def test_return_type(self):

        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        assert type(confusion_matrix) == ConfusionMatrix

    def test_data_structure_type(self):
        
        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])
        
        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        assert type(confusion_matrix.matrix) == pd.core.frame.DataFrame

    def test_increment_matrix_one_currency_with_one_signal(self):
        
        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])
        
        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        assert confusion_matrix.matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1

    def test_increment_matrix_one_currency_with_two_signals(self):
        
        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }
        ])
        
        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        assert confusion_matrix.matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 1

    def test_increment_matrix_one_currency_with_three_signals(self):
        
        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": 1588507200,
            "execution_id": 3
        }
        ])
        
        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        assert confusion_matrix.matrix.loc["NEUTRAL_PRED", "SHORT_TRUE"] == 1

    def test_addition_confusion_matrix(self):
        strategies_results_for_crypto = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.NEUTRAL,
            "add_date": 1588507200,
            "execution_id": 3
        }
        ])
        
        confusion_matrix_generator = ConfusionMatrixGenerator(strategies_results_for_crypto, self.crypto_pair)
        confusion_matrix = confusion_matrix_generator.generate()
        confusion_matrix += confusion_matrix
        assert confusion_matrix.matrix.loc["LONG_PRED", "SHORT_TRUE"] == 2
        assert confusion_matrix.matrix.loc["NEUTRAL_PRED", "SHORT_TRUE"] == 2
        assert confusion_matrix.matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 2

    def test_strategies_results_sorter_input(self):
        
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results]

        StrategiesResultsSorter(list_strategies_results)

    def test_strategies_results_sorter_output(self):
        
        strategies_results_1 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        strategies_results_2 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results_1, strategies_results_2]

        strategies_results_sorted = StrategiesResultsSorter(list_strategies_results).sort()
        for strategies_results in strategies_results_sorted:
            assert strategies_results.results[0].currency_pair.name == strategies_results.results[1].currency_pair.name

    def test_book_of_confusion_matrixes_input(self):
        
        strategies_results_1 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        strategies_results_2 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results_1, strategies_results_2]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair, self.crypto_pair_bis])
        BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)

    def test_book_of_confusion_matrixes_output_one_signal(self):
        
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        list_strategies_results = [strategies_results]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair])
        book = BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)
        book.write()
        book_of_confusion_matrixes = book.read()
        assert book_of_confusion_matrixes[0].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"
        assert len(book_of_confusion_matrixes) == len(crypto_currency_pairs.pairs)

    def test_book_of_confusion_matrixes_output_two_signals(self):
        
        strategies_results_1 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        strategies_results_2 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results_1, strategies_results_2]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair])
        book = BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)
        book.write()
        book_of_confusion_matrixes = book.read()
        assert book_of_confusion_matrixes[0].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"
        assert book_of_confusion_matrixes[0].matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"
        assert len(book_of_confusion_matrixes) == len(crypto_currency_pairs.pairs)

    def test_book_of_confusion_matrixes_output_two_currencies_each_one_signal(self):
        
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair, self.crypto_pair_bis])
        book = BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)
        book.write()
        book_of_confusion_matrixes = book.read()
        assert book_of_confusion_matrixes[0].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"
        assert book_of_confusion_matrixes[0].matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 0
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"

        assert book_of_confusion_matrixes[1].matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[1].crypto_currency_pair.name == "ETH_EUR"
        assert book_of_confusion_matrixes[1].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 0
        assert book_of_confusion_matrixes[1].crypto_currency_pair.name == "ETH_EUR"
        assert len(book_of_confusion_matrixes) == len(crypto_currency_pairs.pairs)

    def test_book_of_confusion_matrixes_output_two_currencies_each_two_signals(self):
        
        strategies_results_1 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        strategies_results_2 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results_1, strategies_results_2]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair, self.crypto_pair_bis])
        book = BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)
        book.write()
        book_of_confusion_matrixes = book.read()
        assert book_of_confusion_matrixes[0].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"
        assert book_of_confusion_matrixes[0].matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 1
        assert book_of_confusion_matrixes[0].crypto_currency_pair.name == "BTC_EUR"

        assert book_of_confusion_matrixes[1].matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 2
        assert book_of_confusion_matrixes[1].crypto_currency_pair.name == "ETH_EUR"
        assert book_of_confusion_matrixes[1].matrix.loc["LONG_PRED", "SHORT_TRUE"] == 0
        assert book_of_confusion_matrixes[1].crypto_currency_pair.name == "ETH_EUR"
        assert len(book_of_confusion_matrixes) == len(crypto_currency_pairs.pairs)

    def test_book_of_confusion_matrixes_sum(self):
        
        strategies_results_1 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        strategies_results_2 = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        },
        {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.ETH_EUR,
            "signal": Signal.SHORT,
            "add_date": 1588503600,
            "execution_id": 2
        }])

        list_strategies_results = [strategies_results_1, strategies_results_2]
        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair, self.crypto_pair_bis])
        book = BookOfConfusionMatrixes(list_strategies_results, crypto_currency_pairs)
        book.write()
        confusion_matrix_sum = book.get_sum()
        assert confusion_matrix_sum.matrix.loc["LONG_PRED", "LONG_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["LONG_PRED", "NEUTRAL_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["LONG_PRED", "SHORT_TRUE"] == 1

        assert confusion_matrix_sum.matrix.loc["NEUTRAL_PRED", "LONG_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["NEUTRAL_PRED", "NEUTRAL_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["NEUTRAL_PRED", "SHORT_TRUE"] == 0

        assert confusion_matrix_sum.matrix.loc["SHORT_PRED", "LONG_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["SHORT_PRED", "NEUTRAL_TRUE"] == 0
        assert confusion_matrix_sum.matrix.loc["SHORT_PRED", "SHORT_TRUE"] == 3


    #-------------------------------------------------
    # Unhappy paths

    def test_instanciate_class_fails(self):
        with self.assertRaises(Exception):
            ConfusionMatrix({})

    def test_instanciate_class_fails_2(self):
        strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1588500000,
            "execution_id": 1
        }])

        crypto_currency_pairs = CryptoCurrencyPairs([self.crypto_pair])

        strategies_results.results.append(0)
        crypto_currency_pairs.pairs.append([])
        with self.assertRaises(Exception):
            ConfusionMatrixGenerator(strategies_results, crypto_currency_pairs)
        
    def test_instanciate_class_fails_3(self):
        with self.assertRaises(Exception):
            BookOfConfusionMatrixes([3, "", {}])