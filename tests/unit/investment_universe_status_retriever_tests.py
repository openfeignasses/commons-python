import unittest
from unittest.mock import MagicMock, patch
from cryptobot_commons.investment_universe_status import InvestmentUniverseStatus
from cryptobot_commons.repositories import InvestmentUniverseStatusRepository
from cryptobot_commons.retrievers.allocation_results_retriever import AllocationResultsRetriever
from cryptobot_commons.retrievers.investment_universe_status_retriever import InvestmentUniverseStatusRetriever


class InvestmentUniverseRetrieverTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def setUp(cls, mysql_mock):
        cls.investment_universe_status_repository = InvestmentUniverseStatusRepository()
        cls.investment_universe_status_raw = [{
            "name": "BTC_EUR",
            "status": "TRADABLE",
        }, {
            "name": "GNO_EUR",
            "status": "TRADABLE",
        }]
        cls.investment_universe_status_repository.db.cursor().fetchall = MagicMock(
            return_value=cls.investment_universe_status_raw)

    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_mode_all(self):
        investment_universe_status_retriever_all = InvestmentUniverseStatusRetriever("ALL")
        retrieved_investment_universe_status = investment_universe_status_retriever_all.execute()
        self.assertIsInstance(retrieved_investment_universe_status, InvestmentUniverseStatus)

    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_mode_tradable(self):
        investment_universe_status_retriever_tradable = InvestmentUniverseStatusRetriever("TRADABLE")
        retrieved_investment_universe_status = investment_universe_status_retriever_tradable.execute()
        self.assertIsInstance(retrieved_investment_universe_status, InvestmentUniverseStatus)

    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_mode_non_tradable(self):
        investment_universe_status_retriever_tradable = InvestmentUniverseStatusRetriever("NON_TRADABLE")
        retrieved_investment_universe_status = investment_universe_status_retriever_tradable.execute()
        self.assertIsInstance(retrieved_investment_universe_status, InvestmentUniverseStatus)

    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_mode_non_tradable(self):
        investment_universe_status_retriever_tradable = InvestmentUniverseStatusRetriever("CRASHING")
        retrieved_investment_universe_status = investment_universe_status_retriever_tradable.execute()
        self.assertIsInstance(retrieved_investment_universe_status, InvestmentUniverseStatus)

    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked"
    })
    def test_retrieve_fails(self):
        with self.assertRaises(Exception):
            AllocationResultsRetriever("INEXISTANT_MODE")
