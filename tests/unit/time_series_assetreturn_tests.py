import unittest
import datetime
import pandas as pd
import numpy as np
from cryptobot_commons.time_series import TimeSeries


class TimeSeriesAssetReturnTestCase(unittest.TestCase):
    """
    Note: values used in variables "..._we_expect" were generated with the calculation sheet "testsdata_time_series.ods"
    """

    def setUp(self):
        # First we will initialize the pd.Series, which is the quote coming from the market (at an arbitrary date)
        quotes_list = [5, 6, 12, 10, 8, 7, 5, 8, 8, 9, 7, 8, 9,
                       6, 3, 4, 9, 8, 9, 3, 6, 4, 7, 8, 9, 6, 5, 4, 6, 9, 8]
        base = datetime.datetime.strptime("29-12-2019", "%d-%m-%Y")
        dates_list = [base - datetime.timedelta(days=len(
            quotes_list)) + datetime.timedelta(days=i) for i in range(len(quotes_list))]
        serie = pd.Series(quotes_list, index=dates_list, name='BTC_EUR')
        # We can then initialize the TimeSeries object
        self.time_series = TimeSeries("BTC_EUR", serie)

    def test_time_series_asset_return_is_panda_series(self):
        self.assertTrue(type(self.time_series.asset_return(1)) == pd.core.series.Series)

    def test_time_series_asset_return_has_good_resultA(self):
        is_ok = True
        returns = self.time_series.asset_return(1)
        returns_we_expect = [None, 0.2, 1, -0.166666666666667, -0.2, -0.125, -0.285714285714286, 0.6, 0, 0.125,
                             -0.222222222222222, 0.142857142857143, 0.125,
                             -0.333333333333333, -0.5, 0.333333333333333,
                             1.25, -0.111111111111111, 0.125, -0.666666666666667, 1, -0.333333333333333, 0.75,
                             0.142857142857143, 0.125, -0.333333333333333, -0.166666666666667, -0.2, 0.5, 0.5,
                             -0.111111111111111]
        for i in range(len(returns_we_expect)):
            if np.isnan(returns.iloc[i]) and returns_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(returns.iloc[i]) and returns_we_expect[i] is None:
                continue
            elif round(returns.iloc[i], 5) != round(returns_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_asset_return_has_good_resultB(self):
        is_ok = True
        returns = self.time_series.asset_return(3)
        returns_we_expect = [None, None, None, 1, 0.333333333333333, -0.416666666666667, -0.5, 0, 0.142857142857143,
                             0.8, -0.125, 0, 0, -0.142857142857143, -0.625,
                             -0.555555555555556, 0.5, 1.66666666666667,
                             1.25, -0.666666666666667, -0.25, -
                             0.555555555555556, 1.33333333333333, 0.333333333333333,
                             1.25, -0.142857142857143, -0.375, -0.555555555555556, 0, 0.8, 1]
        for i in range(len(returns_we_expect)):
            if np.isnan(returns.iloc[i]) and returns_we_expect[i] is not None:
                is_ok = False
            elif np.isnan(returns.iloc[i]) and returns_we_expect[i] is None:
                continue
            elif round(returns.iloc[i], 5) != round(returns_we_expect[i], 5):
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_asset_return_has_same_index_as_data(self):
        is_ok = True
        returns = self.time_series.asset_return(1)
        data = self.time_series.data
        for i in range(len(data.index)):
            if list(returns.index)[i] != list(data.index)[i]:
                is_ok = False
        self.assertEqual(is_ok, True)

    def test_time_series_asset_return_has_nb_NaN_same_as_periodA(self):
        returns = self.time_series.asset_return(1)
        count_nan = 0
        for el in returns:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 1)

    def test_time_series_asset_return_has_nb_NaN_same_as_periodB(self):
        returns = self.time_series.asset_return(4)
        count_nan = 0
        for el in returns:
            if np.isnan(el):
                count_nan += 1
            else:
                pass
        self.assertTrue(count_nan == 4)
