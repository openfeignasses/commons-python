import unittest
from unittest.mock import patch

from cryptobot_commons.allocation_results import AllocationResults
from cryptobot_commons.savers.allocation_results_saver import AllocationResultsSaver


class AllocationResultsSaverTestCase(unittest.TestCase):

    # Called before each test
    @patch('mysql.connector.connect')
    @patch.dict('os.environ', {
        'DATABASE_HOST': "mocked",
        'MYSQL_USER': "mocked",
        'MYSQL_DATABASE': "mocked",
        'MYSQL_PASSWORD': "mocked",
        'MYSQL_SSL_DISABLED': "mocked",
        'CURRENT_TIMESTAMP': "1234"
    })
    def setUp(cls, mysql_mock):
        cls.allocation_results_saver = AllocationResultsSaver(AllocationResults([{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 1,
            "value_percent": 1.0,
            "add_date": 1589113875
        }]))
        cls.allocation_results_saver2 = AllocationResultsSaver(AllocationResults([{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 1,
            "value_percent": 1.0,
            "add_date": None
        }]))

    def test_save_success(self):
        self.allocation_results_saver.save()

    def test_save_with_date(self):
        allocation_results = self.allocation_results_saver.save().allocation_results
        assert(allocation_results.results[0].add_date.timestamp() == 1589113875)

    def test_save_fails(self):
        self.allocation_results_saver.repository = None
        with self.assertRaises(Exception):
            self.allocation_results_saver.save()

    def test_wrong_input(self):
        with self.assertRaises(Exception):
            AllocationResultsSaver(AllocationResults({}))

    def test_sum_exeption1(self):
        allocation_results_raw = [{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.5,
            "add_date": None
        }, {
            "currency": "LTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.5,
            "add_date": None
        }, {
            "currency": "ETH",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.5,
            "add_date": None
        }]
        with self.assertRaises(Exception):
            AllocationResultsSaver(allocation_results_raw)

    def test_sum_exeption2(self):
        allocation_results_raw = [{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.2,
            "add_date": None
        }, {
            "currency": "LTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.2,
            "add_date": None
        }, {
            "currency": "ETH",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.2,
            "add_date": None
        }]
        with self.assertRaises(Exception):
            AllocationResultsSaver(allocation_results_raw)

    def test_add_date_forced(self):
        allocation_results = self.allocation_results_saver2.save().allocation_results
        assert(allocation_results.results[0].add_date.timestamp() == float(1234))