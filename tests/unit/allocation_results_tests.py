import unittest
from cryptobot_commons.allocation_results import AllocationResults


class AllocationResultsTestCase(unittest.TestCase):

    def setUp(self):
        self.allocation_results_raw = [{
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": 1589113875
        }, {
            "currency": "LTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.25,
            "add_date": None
        }, {
            "currency": "ETH",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.25,
            "add_date": None
        }]

    def test_instaciate_class(self):
        AllocationResults(allocation_results=self.allocation_results_raw)

    def test_wrong_input_type1(self):
        with self.assertRaises(Exception):
            AllocationResults(allocation_results=123)

    def test_wrong_input_type2(self):
        allocation_results_raw = [123, 123, 123]
        with self.assertRaises(Exception):
            AllocationResults(allocation_results=allocation_results_raw)
