import unittest
from cryptobot_commons.allocation_result import AllocationResult
from cryptobot_commons.investment_universe import InvestmentUniverse


class AllocationResultTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def test_instanciate(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": 1589113875
        }
        AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_instanciate_none_values(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": None,
            "value_percent": 0.5,
            "add_date": None
        }
        AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_to_dict(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": 1589113875
        }
        expected_dict = allocation_result_raw

        allocation_result = AllocationResult(allocation_result_raw=allocation_result_raw)
        allocation_result_as_dict = allocation_result.to_dict()

        self.assertEqual(expected_dict, allocation_result_as_dict)

    def test_wrong_type_1(self):
        allocation_result_raw = {
            "currency": InvestmentUniverse.BTC_EUR,
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_wrong_type_2(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": 123,
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_wrong_type_3(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": "100",
            "value_percent": 0.5,
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_wrong_type_4(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": "0.5",
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_wrong_type_5(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 0.5,
            "add_date": "2019-02-20"
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_value_percent_too_large(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": 50,
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)

    def test_value_percent_negative(self):
        allocation_result_raw = {
            "currency": "BTC",
            "allocation_name": "EQUIREPARTITION",
            "execution_id": 100,
            "value_percent": -0.5,
            "add_date": 1589113875
        }
        with self.assertRaises(Exception):
            AllocationResult(allocation_result_raw=allocation_result_raw)
