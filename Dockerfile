FROM python:3.7

RUN apt update && apt install zip

ADD requirements.txt requirements.txt

RUN pip install -r requirements.txt

ADD . .

CMD ./launch_unit_tests.sh